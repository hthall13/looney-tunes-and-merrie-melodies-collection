import re
import json
import requests
from datetime import datetime
from bs4 import BeautifulSoup
from unidecode import unidecode

def get_characters(episode_number):
  char_dicts = [
    {'name': 'A. Flea', 'episodes': [417, 510]},
    {'name': 'Ape Waiter', 'episodes': [13, 30]},
    {'name': 'Aunt Jemima', 'episodes': [186, 409]},
    {'name': 'Alley Cat', 'episodes': [431, 594, 599, 620]},
    {'name': 'Adolf Hitler', 'episodes': [64, 159, 336, 368, 376, 378, 379, 392, 396, 403, 409, 410, 416, 429, 440, 447]},
    {'name': 'Agatha and Emily Vulture', 'episodes': [925]},
    {'name': 'Ala Bahma', 'episodes': [390]},
    {'name': 'Alice Crumden', 'episodes': [786, 796, 868]},
    {'name': 'Angus MacRory', 'episodes': [542]},
    {'name': 'Babbit and Catstello', 'episodes': [387, 456, 471, 485]},
    {'name': 'Babyface Finster', 'episodes': [727]},
    {'name': 'Banty Rooster', 'episodes': [919]},
    {'name': 'Barnyard Dawg', 'episodes': [481, 489, 501, 536, 551, 560, 588, 626, 629, 649, 661, 687, 695, 732, 762, 797, 813, 821, 826, 830, 907, 919]},
    {'name': 'Beaky Buzzard', 'episodes': [373, 460, 582, 585]},
    {'name': 'Mama Buzzard', 'episodes': [373, 460]},
    {'name': 'Beans', 'episodes': [100, 109, 113, 115, 116, 119, 121, 123, 125, 126, 129]},
    {'name': 'Ben Birdie', 'episodes': [148, 184]},
    {'name': 'Benny', 'episodes': [656, 688]},
    {'name': 'Big Bad Wolf', 'episodes': [182, 281, 287, 324, 393, 419, 439, 464, 565, 653, 753, 788, 822, 936]},
    {'name': 'Bingo Crosbyana', 'episodes': [130, 133]},
    {'name': 'Blacque Jacque Shellacque', 'episodes': [849, 895]},
    {'name': 'Bobo the Elephant', 'episodes': [497, 720]},
    {'name': 'Bosko', 'episodes': [0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 15, 17, 19, 21, 23, 25, 27, 29, 31, 34, 36, 38, 39, 41, 43, '45a', '45b', 47, 49, 51, 53, 56, 58, 59, 61, 63, 64]},
    {'name': 'Bratty Kid', 'episodes': [500]},
    {'name': 'Bruno', 'episodes': [21, 27, 31, 34, 36, '45a', 47, 49, 56, 58, 63, 64]},
    {'name': 'Buddy', 'episodes': [66, 68, 70, 72, 74, 75, 78, 80, 82, 87, 89, 90, 92, 94, 95, 96, 97, 98, 101, 102, 105, 106, 108, 110]},
    {'name': 'Bugs Bunny', 'episodes': [198, 236, 253, 277, 294, 296, 311, 319, 329, 333, 340, 350, 359, 360, 365, 369, 373, 378, 386, 390, 394, 399, 405, 407, 408, 412, 414, 419, 420, 423, 427, '433a', '433b', 435, 438, 443, 445, 446, 447, 449, 453, 458, 462, 465, 469, 473, 476, 482, 484, 487, 490, 493, 496, 500, 507, 511, 513, 520, 523, 525, 529, 532, 534, 537, 542, 545, 549, 552, 554, 557, 559, 561, 563, 565, 569, 574, 576, 579, 581, 582, 584, 587, 591, 593, 596, 600, 604, 607, 609, 612, 615, 618, 621, 624, 627, 631, 632, 635, 637, 639, 642, 644, 647, 651, 655, 660, 664, 667, 671, 673, 674, 678, 682, 686, 691, 692, 697, 700, 705, 709, 713, 716, 719, 727, 731, 735, 739, 742, 744, 748, 751, 754, 759, 763, 768, 772, 775, 777, 780, 783, 787, 791, 794, 798, 801, 805, 809, 812, 815, 818, 822, 825, 829, 833, 836, 839, 842, 846, 849, 852, 855, 858, 861, 864, 869, 875, 883, 887, 889, 895, 901, 909, 912, 914, 918, 921, 923, 925, 927, 931, 933, 936]},
    {'name': 'Bunny and Claude', 'episodes': [994, 995]},
    {'name': 'Casper Caveman', 'episodes': [239]},
    {'name': 'Cecil Turtle', 'episodes': [319, 394, 496, 528, 556, 713]},
    {'name': 'Character Guide', 'episodes': []},
    {'name': 'Charlie Dog', 'episodes': [505, 547, 564, 597, 619, 824]},
    {'name': 'Chester', 'episodes': [662, 710]},
    {'name': 'Clarence Cat', 'episodes': [803]},
    {'name': 'Claude Cat', 'episodes': [406, 413, 446, 488, 553, 586, 608, 628, 659, 665, 702, 704, 832, 904]},
    {'name': 'Claude Hopper', 'episodes': [398]},
    {'name': 'Clyde Bunny', 'episodes': [627, 719]},
    {'name': 'Colonel', 'episodes': [455]},
    {'name': 'Colonel Rimfire', 'episodes': [981, 986, 988, 991]},
    {'name': 'Colonel Shuffle', 'episodes': [549, 597]},
    {'name': 'Conrad the Cat', 'episodes': [354, 357, 358]},
    {'name': 'Construction Worker', 'episodes': [758]},
    {'name': 'Cookie', 'episodes': [66, 68, 70, 75, 78, 82, 87, 89, 92, 94, 95, 97, 98, 101, 108]},
    {'name': 'Cool Cat', 'episodes': [981, 986, 988, 991, 999, 1000]},
    {'name': 'Cornbread', 'episodes': []},
    {'name': 'Cottontail Smith', 'episodes': [399]},
    {'name': 'Count Bloodcount', 'episodes': [925]},
    {'name': 'The Crusher', 'episodes': [520, 615]},
    {'name': 'Daffy Duck', 'episodes': [160, 187, 192, 209, 222, 223, 239, 247, 252, 260, 271, 286, 328, 338, 358, 364, 381, 385, 389, 395, 401, 404, 408, 410, 412, 416, 422, 426, 430, 432, 440, 444, 448, 454, 463, 464, 468, 470, 475, 478, 494, 498, 502, 508, 514, 516, 527, 530, 538, 540, 541, 544, 546, 551, 580, 583, 589, 595, 598, 621, 634, 637, 643, 654, 660, 663, 666, 673, 677, 685, 692, 707, 724, 725, 731, 733, 735, 744, 749, 762, 764, 774, 780, 781, 791, 800, 804, 809, 813, 817, 835, 839, 855, 861, 883, 890, 898, 908, 913, 914, 922, 933, 940, 943, 944, 945, 946, 947, 950, 953, 956, 959, 962, 964, 966, 967, 968, 969, 970, 971, 973, 974, 975, 976, 977, 978, 979, 980, 983, 987, 990]},
    {'name': 'Dan Backslide', 'episodes': [382]},
    {'name': 'The Devil', 'episodes': [79, 139, 146, 163, 717, 737, 912, 916]},
    {'name': 'Dizzy Duck', 'episodes': [229, 256]},
    {'name': 'Dodsworth', 'episodes': [646, 675]},
    {'name': 'Dr. Lorre', 'episodes': [473, 494]},
    {'name': 'Dr. Moron', 'episodes': [647]},
    {'name': 'Dr. Oro Myicin', 'episodes': [739]},
    {'name': 'The Drunk Stork', 'episodes': [468, 670, 721, 733, 757, 839]},
    {'name': 'Drunken Bull', 'episodes': [155]},
    {'name': 'Duck Dodgers', 'episodes': [685]},
    {'name': 'Dwarf Eagle', 'episodes': [847]},
    {'name': 'Eager Young Space Cadet', 'episodes': [685]},
    {'name': 'Easter Rabbit', 'episodes': [500]},
    {'name': 'Eggbert', 'episodes': [712, 729, 865]},
    {'name': 'Egghead', 'episodes': [169, 187, 212, 225]},
    {'name': 'Elmer Fudd', 'episodes': [182, 201, 207, 214, 218, 230, 234, 244, 277, 280, 284, 294, 303, 311, 350, 360, 365, 368, 378, 386, 395, 404, 412, 417, 443, 444, 445, 449, 462, 469, 484, 500, 502, 507, 514, 518, 539, 544, 545, 568, 583, 591, 607, 621, 660, 674, 679, 692, 697, 707, 724, 730, 731, 739, 742, 744, 755, 759, 780, 782, 783, 801, 812, 813, 829, 841, 861, 873, 891, 899]},
    {'name': 'Elmo the Mouse', 'episodes': [517]},
    {'name': 'Emily the Chicken', 'episodes': [130, 145, 195]},
    {'name': 'Evil Giant Robot', 'episodes': [647]},
    {'name': 'Fauntleroy Flip', 'episodes': [720]},
    {'name': 'Fernando', 'episodes': [802, 848, 888, 956]},
    {'name': 'Fluffy', 'episodes': [18, 20]},
    {'name': 'Foghorn Leghorn', 'episodes': [481, 501, 536, 560, 588, 599, 626, 629, 649, 661, 687, 695, 712, 729, 732, 761, 762, 778, 797, 821, 826, 850, 865, 870, 880, 903, 907, 919, 936]},
    {'name': 'Fox', 'episodes': [797]},
    {'name': 'Foxy', 'episodes': [13, 14, 16]},
    {'name': "Frankenstein's monster", 'episodes': [115]},
    {'name': 'Frisky Puppy', 'episodes': [608, 665, 704]},
    {'name': 'Gabby Goat', 'episodes': [162, 170, 174, 204]},
    {'name': 'The Gambling Bug', 'episodes': [620]},
    {'name': 'The Gashouse Gorillas Baseball Team', 'episodes': [465]},
    {'name': 'George K. Chickenhawk', 'episodes': [481, 530, 536]},
    {'name': 'George the Fox', 'episodes': [308]},
    {'name': 'Gertrude', 'episodes': [737]},
    {'name': 'Giant', 'episodes': [405]},
    {'name': 'Giovanni Jones', 'episodes': [559]},
    {'name': 'Goldimouse', 'episodes': [860]},
    {'name': 'Goofy Gophers', 'episodes': [490, 512, 577, 617, 701, 730, 743, 830, 950]},
    {'name': 'Goopy Geer', 'episodes': [30, 33, 35, 49]},
    {'name': 'Gossamer', 'episodes': [473, 647, 685, 836]},
    {'name': 'Gracie', 'episodes': [310, 603]},
    {'name': 'Granny', 'episodes': [182, 348, 411, 601, 816]},
    {'name': 'The Gremlin', 'episodes': [414, 429]},
    {'name': 'Grover Groundhog', 'episodes': [489]},
    {'name': 'Gruesome Gorilla', 'episodes': [511, 579, 839]},
    {'name': 'Gwendolyn', 'episodes': [585]},
    {'name': 'Ham and Ex', 'episodes': [100, 121, 126, 129]},
    {'name': 'Hatta Mari', 'episodes': [440]},
    {'name': 'Heathcliff', 'episodes': [531]},
    {'name': 'Hector the Bulldog', 'episodes': [461, 493, 519, 578, 592, 602, 622, 641, 648, 652, 662, 676, 689, 699, 710, 714, 717, 747, 757, 760, 792, 793, 807, 832, 845, 882, 888, 913, 941]},
    {'name': 'Henery Hawk', 'episodes': [377, 481, 501, 530, 536, 560, 583, 588, 626, 661, 732, 880]},
    {'name': 'Hippety Hopper', 'episodes': [310, 573, 603, 604, 638, 656, 696, 708, 734, 760, 779, 810, 878, 930]},
    {'name': 'Homer Pigeon', 'episodes': [440]},
    {'name': 'Honey', 'episodes': [1, 3, 6, 10, 11, 19, 23, 25, 29, 34, 36, 38, 39, 41, 43, '45a', 49, 51, 53, 56, 58, 59, 61, 63, 64]},
    {'name': 'Horton', 'episodes': [362]},
    {'name': 'Hubie and Bertie', 'episodes': [406, 450, 488, 504, 553, 586, 628]},
    {'name': 'Hugo the Abominable Snowman', 'episodes': [883]},
    {'name': 'Inki', 'episodes': [261, 334, 415, 499, 605]},
    {'name': 'Instant Martians', 'episodes': [746, 818]},
    {'name': 'Inuit Hunter', 'episodes': [569]},
    {'name': 'J.P. Cubish', 'episodes': [538]},
    {'name': 'Johnny Cat', 'episodes': [203]},
    {'name': 'John Rooster', 'episodes': [568]},
    {'name': 'Jose and Manuel', 'episodes': [785, 844, 876, 899, 953, 962]},
    {'name': 'Junior', 'episodes': [820]},
    {'name': 'K-9', 'episodes': [529, 651]},
    {'name': 'Kid Banty', 'episodes': [649]},
    {'name': 'Kitty Bright', 'episodes': [203]},
    {'name': 'Laramore', 'episodes': [395]},
    {'name': 'Leo the Lion', 'episodes': [180, 369, 582]},
    {'name': 'Little John', 'episodes': [576]},
    {'name': 'Little Kitty', 'episodes': [100, 115, 116, 119, 125, 129]},
    {'name': 'Little Red Riding Hood', 'episodes': [182, 281, 324, 419, 439, 464, 648, 753]},
    {'name': 'Little Yellow Duck', 'episodes': [454]},
    {'name': 'Loyal Order Grand Master', 'episodes': [708]},
    {'name': 'Malcolm Falcon', 'episodes': [939]},
    {'name': 'Mama Buzzard', 'episodes': [373, 460]},
    {'name': 'Marc Anthony and Pussyfoot', 'episodes': [628, 640, 672, 702, 704, 832]},
    {'name': 'The Martin Brothers', 'episodes': [596]},
    {'name': 'Marvin the Martian', 'episodes': [529, 651, 685, 818, 923]},
    {'name': 'Melissa Duck', 'episodes': [463, 583, 663, 677]},
    {'name': 'Merlin of Monroe', 'episodes': [751]},
    {'name': 'Merlin the Magic Mouse', 'episodes': [982, 984, 989, 996, 998]},
    {'name': 'Michigan J. Frog', 'episodes': [758]},
    {'name': 'Minah Bird', 'episodes': [261, 334, 415, 497, 499, 605]},
    {'name': 'Miss Prissy', 'episodes': [590, 629, 695, 712, 729, 850, 880]},
    {'name': 'Monte', 'episodes': [585]},
    {'name': 'Mot', 'episodes': [776]},
    {'name': 'Mr. Hook', 'episodes': [262]},
    {'name': 'Mr. Meek', 'episodes': [401]},
    {'name': 'Mr. Viper', 'episodes': [144]},
    {'name': 'Mrs. Cat', 'episodes': [670, 860]},
    {'name': 'Mrs. Daffy Duck', 'episodes': [252, 338, 444, 589, 733, 898]},
    {'name': 'Mrs. Elmer Fudd', 'episodes': [813]},
    {'name': 'Narrator', 'episodes': []},
    {'name': 'Nasty Canasta', 'episodes': [634, 725, 775]},
    {'name': 'Ned Morton', 'episodes': [786, 796, 868]},
    {'name': 'Nelly the Giraffe', 'episodes': [894]},
    {'name': 'Nick', 'episodes': [595, 694]},
    {'name': "O'Pat and O'Mike", 'episodes': [625]},
    {'name': 'Oliver Owl', 'episodes': [100, 115, 119, 255, 293]},
    {'name': 'Owl Jolson', 'episodes': [137]},
    {'name': 'Pablo', 'episodes': [802, 888, 920, 956]},
    {'name': 'Pappy and Elvis', 'episodes': [842, 870]},
    {'name': 'Penelope Pussycat', 'episodes': [572, 616, 645, 706, 740, 752, 766, 808, 843, 862, 885, 904]},
    {'name': 'Pepé Le Pew', 'episodes': [446, 483, 492, 535, 572, 616, 645, 684, 699, 706, 740, 752, 766, 808, 843, 853, 862, 885, 904]},
    {'name': 'Pete Puma', 'episodes': [664, 891]},
    {'name': 'Petunia Pig', 'episodes': [158, 181, 183, 247, 249, 260, 408, 927]},
    {'name': 'Phineas Pig', 'episodes': [138, 144, 188, 240]},
    {'name': 'Piggy', 'episodes': [18, 20, 22, 24, 26, 28, 30, 32, 33, 35, 37]},
    {'name': 'Piggy Hamhock', 'episodes': [141, 153]},
    {'name': 'Pinky Pig', 'episodes': [217, 249]},
    {'name': 'Playboy Penguin', 'episodes': [569, 593]},
    {'name': 'Porky Pig', 'episodes': [100, 109, 115, 116, 119, 121, 123, 125, 127, 129, 132, 134, 136, 138, 140, 142, 144, 145, 147, 149, 150, 152, 154, 155, 158, 160, 162, 165, 168, 170, 172, 174, 176, 179, 181, 183, 185, 188, 190, 192, 194, 196, 198, 200, 202, 204, 208, 209, 211, 215, 217, 219, 222, 224, 227, 229, 232, 235, 237, 240, 242, 245, 247, 248, 249, 252, 253, 256, 259, 260, 263, 266, 269, 271, 273, 276, 278, 282, 283, 286, 289, 292, 296, 299, 301, 304, 307, 309, 312, 320, 322, 323, 325, 328, 330, 332, 336, 338, 341, 344, 347, 351, 353, 356, 357, 389, 392, 404, 408, 412, 417, 422, 426, 428, 430, 432, 434, 457, 450, 464, 468, 470, 474, 478, 486, 489, 505, 516, 522, 533, 541, 543, 547, 548, 550, 551, 556, 564, 566, 571, 580, 583, 590, 595, 598, 606, 625, 634, 637, 643, 654, 666, 685, 711, 725, 746, 749, 764, 781, 800, 817, 835, 890, 927, 947, 962]},
    {'name': "Porky's Mother", 'episodes': [123, 160, 185, 211, 269]},
    {'name': 'Prof. Calvin Q. Calculus', 'episodes': [737]},
    {'name': 'Proto-Granny', 'episodes': [182, 324, 348, 411, 435, 565]},
    {'name': 'Quick Brown Fox', 'episodes': [997]},
    {'name': 'Ralph Crumden', 'episodes': [786, 796, 868]},
    {'name': 'Ralph Phillips', 'episodes': [723, 795]},
    {'name': 'Ralph Wolf and Sam Sheepdog', 'episodes': [668, 726, 745, 799, 867, 896, 917]},
    {'name': 'Rapid Rabbit', 'episodes': [997]},
    {'name': 'Rhode Island Red', 'episodes': [778]},
    {'name': 'Road Runner', 'episodes': [567, 639, 650, 657, 690, 718, 738, 756, 769, 784, 787, 790, 806, 819, 828, 831, 840, 851, 856, 864, 871, 877, 884, 887, 892, 902, 918, 926, 934, 942, 948, 949, 951, 952, 954, 955, 957, 958, 960, 961, 963, 965, 972]},
    {'name': 'Robin Hood', 'episodes': [46, 205, 231, 576, 817, 962]},
    {'name': 'Rocky and Mugsy', 'episodes': [482, 595, 694, 705, 717, 772, 805, 921]},
    {'name': 'Rosebud', 'episodes': [326, 362]},
    {'name': 'Roxy', 'episodes': [13, 14, 16]},
    {'name': 'Russian Dog', 'episodes': ['433a']},
    {'name': 'Sam Cat', 'episodes': [613, 765, 786, 796, 837, 866, 977, 982]},
    {'name': 'Sam Crubish', 'episodes': [852]},
    {'name': 'Sambo', 'episodes': [431]},
    {'name': 'Santa Claus', 'episodes': [22, 48, 207, 306]},
    {'name': 'Schnooks', 'episodes': [437, 455]},
    {'name': 'Schultz', 'episodes': [416]},
    {'name': 'Señor Vulturo', 'episodes': [814, 962]},
    {'name': 'Shapely Lady Duck', 'episodes': [663]},
    {'name': 'Shep', 'episodes': [524, 557]},
    {'name': 'Sheriff of Nottingham', 'episodes': [576]},
    {'name': 'Shropshire Slasher', 'episodes': [781]},
    {'name': 'Sloppy Moe', 'episodes': [200, 457]},
    {'name': 'Slowpoke Rodriguez', 'episodes': [844, 900]},
    {'name': 'Smokey the Genie', 'episodes': [537]},
    {'name': 'Sniffles', 'episodes': [243, 255, 267, 285, 293, 306, 314, 322, 342, 400, 441, 472]},
    {'name': 'Speedy Gonzales', 'episodes': [688, 750, 802, 811, 814, 844, 848, 857, 876, 888, 900, 915, 920, 928, 932, 938, 939, 940, 941, 942, 943, 944, 945, 953, 956, 959, 962, 964, 966, 967, 968, 969, 970, 971, 973, 974, 975, 976, 977, 978, 979, 980, 983, 987, 990]},
    {'name': 'Spike', 'episodes': [820]},
    {'name': 'Spoiled King', 'episodes': [909]},
    {'name': 'Spooky', 'episodes': [986]},
    {'name': 'Sylvester', 'episodes': [451, 461, 474, 495, 501, 506, 509, 518, 519, 521, 539, 543, 558, 562, 573, 578, 583, 592, 601, 602, 603, 611, 613, 622, 630, 636, 638, 641, 648, 652, 656, 658, 662, 669, 670, 676, 683, 689, 694, 696, 699, 708, 710, 711, 714, 717, 722, 734, 736, 741, 746, 747, 750, 753, 755, 757, 760, 765, 770, 771, 773, 779, 782, 789, 793, 803, 807, 810, 811, 816, 827, 837, 845, 847, 848, 854, 857, 860, 863, 866, 872, 876, 878, 881, 882, 886, 888, 893, 897, 900, 906, 915, 920, 924, 928, 930, 932, 935, 939, 940, 941, 942, 973]},
    {'name': 'Sylvester Junior', 'episodes': [603, 638, 696, 760, 779, 810, 847, 854, 860, 881, 897, 924, 930]},
    {'name': 'Tasmanian Devil', 'episodes': [713, 794, 804, 901, 931]},
    {'name': 'Tasmanian She-Devil', 'episodes': [713, 794]},
    {'name': 'Teeny the Elephant', 'episodes': [698, 853]},
    {'name': "Tex's Coon", 'episodes': [340]},
    {'name': 'The Dog', 'episodes': [490, 512, 577]},
    {'name': 'The Fat Horse', 'episodes': [801]},
    {'name': 'The Kitten', 'episodes': [646, 675]},
    {'name': 'The Little Man from the Draft Board', 'episodes': [448]},
    {'name': 'The Supreme Cat', 'episodes': [550, 570, 594, 599, 620, 626]},
    {'name': 'The Woodpecker', 'episodes': [461, 675]},
    {'name': 'Three Bears', 'episodes': [423, 515, 555, 575, 583, 632]},
    {'name': 'Three Little Pigs', 'episodes': [287, 393, 565, 653, 788]},
    {'name': 'Toro the Bull', 'episodes': [686, 915]},
    {'name': 'Trixie Morton', 'episodes': [868]},
    {'name': 'Tom Turk', 'episodes': [422, 546]},
    {'name': 'Tweety', 'episodes': [387, 437, 455, 495, 519, 562, 578, 592, 601, 613, 622, 630, 636, 641, 652, 658, 669, 676, 683, 689, 694, 699, 704, 714, 717, 736, 741, 753, 755, 765, 770, 773, 789, 793, 803, 807, 816, 827, 837, 845, 854, 863, 872, 886, 893, 906, 935]},
    {'name': 'The Two Curious Puppies', 'episodes': [228, 236, 270, 300, 339, 363]},
    {'name': 'Uncle Tom', 'episodes': [20, 46, 164]},
    {'name': 'Von Vultur', 'episodes': [416]},
    {'name': 'W.C. Squeals', 'episodes': [141, 148, 213]},
    {'name': 'The Weasel', 'episodes': [687, 761, 826]},
    {'name': 'Wilber', 'episodes': [19, 38]},
    {'name': 'Wile E. Coyote', 'episodes': [567, 639, 650, 657, 690, 718, 738, 756, 769, 784, 787, 790, 806, 819, 828, 831, 840, 851, 856, 864, 871, 877, 884, 887, 892, 902, 918, 926, 934, 942, 948, 949, 951, 952, 954, 955, 957, 958, 960, 961, 963, 965, 972]},
    {'name': 'Yosemite Sam', 'episodes': [453, 498, 523, 525, 554, 581, 587, 600, 612, 618, 631, 644, 667, 678, 682, 700, 735, 744, 754, 768, 780, 798, 825, 836, 846, 858, 869, 875, 889, 905, 909, 912, 927]},
    {'name': 'Yoyo Dodo', 'episodes': [215, 566]}
  ]
  characters = []
  if episode_number != "#":
    episode_number = int(episode_number)
    for char_dict in char_dicts:
      if episode_number in char_dict["episodes"]:
        characters.append(char_dict["name"])
  return characters

def get_series(year):
  if year == "Date":
    return ""
  elif int(year) < 1960:
    return "Hanna-Barbera/MGM"
  elif int(year) < 1963:
    return "Gene Deitch/Rembrandt Films"
  else:
    return "Chuck Jones/Sib Tower 12"

if __name__ == '__main__':

  base_archive_url = "https://archive.org"
  base_wiki_url = "https://looneytunes.fandom.com"
  wiki_url = base_wiki_url + '/wiki/Looney_Tunes_and_Merrie_Melodies_filmography'
  wiki_html_text = requests.get(wiki_url).text
  wiki_soup = BeautifulSoup(wiki_html_text, 'html.parser')
  video_url = base_archive_url + '/details/everyltmmever_202212'
  video_html_text = requests.get(video_url).text
  video_soup = BeautifulSoup(video_html_text, 'html.parser')
  thumbnail_url = base_archive_url + "/download/everyltmmever_202212/everyltmmever_202212.thumbs/"
  thumbnail_html_text = requests.get(thumbnail_url).text
  thumbnail_soup = BeautifulSoup(thumbnail_html_text, 'html.parser')

  arr = []

  file_name_formats = [
    "[0-9]{4}E[0-9]{2}[a-b]*\.* .+ *\(.+\)\([0-9]+[a-b]*\)\[.+\]\.mp4", # (779) 1933E09. Bosko's Knight-Mare (Bosko, Bruno, Honey)(56)[VHS].mp4
    "[0-9]{4}E[0-9]{2}[a-b]*\.* .+ *\([0-9]+\)\[.+\]\.mp4", # (203) 1933E10. I Like Mountain Music (57)[HBO Max].mp4
    "[0-9]{4}E[0-9]{2}[a-b]*\.* .+ *\(.+\)\([0-9]+\)\[.+\]\(.+\)\.mp4", # (19) 1939E02. Dog Gone Modern (Puppies)(228)[HBO Max](ADDED MISSING DIALOUGE).mp4
    "[0-9]{4}E[0-9]{2}[a-b]*\.* .+ *\([0-9]+\)\[.+\]\(.+\)\.mp4", # (3) 1942E33. The Sheepish Wolf (384)[MeTV](TITLES RESTORED).mp4
    "[0-9]{4}E[0-9]{2}[a-b]*\.* .+ *\(.+\)\([0-9]+\)\[.+\]\[.+\]\(.+\)\.mp4", # (1) 1950E28. Caveman Inki (Inki)(605)[Blu-Ray][LQ](ORIGINAL TITLES).mp4
    "[0-9]{4}E[0-9]{2}[a-b]*\.* .+ *\(.+\)\([0-9]+[a-b]*\)\(.+\)\[.+\]\.mp4", # (1) 1944E15b. Hare Ribbin' (Bugs)(433b)(Director's Cut)[Blu-Ray].mp4
  ]
  botched_episode_names = ["1932E23a", "1939E18", "1940E22.", "1941E16", "1941E25"]
  videos = video_soup.find_all('a', class_="stealth download-pill", attrs={"href": re.compile(".mp4")})
  print("parsing video urls (%d entries)..." % len(videos))
  for video in videos:
    # videofile = str(video.contents[0])
    video_entry = video.text
    try:
      for format in file_name_formats:
        if re.search(format, video_entry) != None:
          videofile = re.search(format, video_entry).group()
          # number
          numberpos = -1
          if (
            file_name_formats.index(format) in [2,3,4,5]
            or any(name in videofile for name in botched_episode_names)
          ):
            numberpos = -2
          if any(name in videofile for name in ["1941E20", "1948E28"]):
            numberpos = -3
          video_number = videofile.split("(")[numberpos].split(")")[0]
          if next((episode for episode in arr if episode["number"] == video_number), None) is None:
            # year
            video_year = videofile[0:4]
            # year_number
            video_year_number = videofile[5:7]
            # characters
            video_characters = get_characters(video_number)
            # if file_name_formats.index(format) in [0,2,4,5]:
            #   video_characters_pre = videofile.split("(")[1].split(")")[0].split(",")
            #   for character in video_characters_pre:
            #     if character[0] == " ":
            #       video_characters.append(character[1:])
            #     else:
            #       video_characters.append(character)
            # title
            if "1955E11" not in videofile:
              video_title = videofile[videofile.find(" ")+1:videofile.find("(")-1]
            else:
              video_title = videofile[videofile.find(" ")+1:videofile.find("(")]
            arr.append({
              "year": video_year,
              "year_number": video_year_number,
              "number": video_number,
              "title": video_title,
              "date": None,
              "summary": None,
              "public_domain": None,
              "blue_ribbon": None,
              "censored_eleven": None,
              "series": None,
              "characters": video_characters,
              "producers": None,
              "wiki": None,
              "video": base_archive_url + video["href"],
              "thumbnail": ""
            })
    except Exception as e:
      print(videofile)
      print(e)
  print("generated %d video entries" % len(arr))

  thumbnail_count = 0
  thumbnails = thumbnail_soup.find_all('a', attrs={"href": re.compile(".jpg")})
  print("parsing thumbnail urls (%d entries)..." % len(thumbnails))
  for thumbnail in thumbnails:
    thumbnailfile = str(thumbnail.contents[0])
    try:
      # year
      thumbnail_year = thumbnailfile[0:4]
      # year_number
      thumbnail_year_number = thumbnailfile[5:7]
      for video in arr:
        if video["year"] == thumbnail_year and video["year_number"] == thumbnail_year_number:
          video["thumbnail"] = thumbnail_url + thumbnail["href"]
          thumbnail_count += 1
    except Exception as e:
      print(thumbnailfile)
      print(e)
  print("modified %d video entries" % thumbnail_count)

  # <li>
  #   <a href="/wiki/Merrie_Melodies" title="Merrie Melodies">
  #     <img alt="Merrie Melodies" src="" decoding="async" loading="lazy" width="12" height="12" data-image-name="Music note.png" data-image-key="Music_note.png" data-src="" class="lazyload">
  #   </a>
  #   "<a href="/wiki/West_of_the_Pesos" title="West of the Pesos">West of the Pesos</a>" (McKimson/Jan 23/<i><a href="/wiki/Looney_Tunes_Golden_Collection:_Volume_4" title="Looney Tunes Golden Collection: Volume 4">4:3</a></i>)
  # </li>
  # <li>
  #   <a href="/wiki/Looney_Tunes" title="Looney Tunes">
  #     <img alt="Looney Tunes" src="" decoding="async" loading="lazy" width="12" height="12" data-image-name="Lt icon.png" data-image-key="Lt_icon.png" data-src="" class="lazyload">
  #   </a> "<a href="" title="Ain't Nature Grand!">Ain't Nature Grand!</a>" (Harman and Ising/Mar)<a href="" title="Public Domain"><img alt="Public Domain" src="" decoding="async" loading="lazy" width="14" height="15" data-image-name="Publicdomain icon.png" data-image-key="Publicdomain_icon.png" data-src="" class="lazyload"></a>
  # </li>
  info = None
  cur_pd = False
  cur_br = False
  cur_c11 = False
  cur_date = None
  cur_title = None
  cur_series = None
  cur_episode = None
  cur_producers = None
  wiki_episodes = []
  pd_count = 0
  br_count = 0
  c11_count = 0
  wiki_list_count = 0
  series_links = ["Looney Tunes", "Merrie Melodies", "Norman Normal"]
  
  print("parsing wiki entries...")

  ordered_lists = wiki_soup.find_all('ol')
  for ordered_list in ordered_lists:
    list_items = ordered_list.find_all('li')
    for item in list_items:
      possible_episodes = item.find_all('a')
      if possible_episodes != None:
        for possibility in possible_episodes:

          cur = possibility.get("title")
          if cur is not None and cur != "":
            if cur in series_links:
              cur_series = possibility["href"].split("/")[-1].replace("_", " ")
            elif cur == "Public Domain":
              cur_pd = True
              pd_count += 1
            elif cur == "Blue Ribbon":
              cur_br = True
              br_count += 1
            elif cur == "Censored Eleven":
              cur_c11 = True
              c11_count += 1
            else:
              found = False
              for entry in arr:
                new_entry = entry["title"].replace("'", "").replace("!", "").replace("-", " ").replace(",", "")
                new_cur = cur.replace("'", "").replace("!", "").replace("-", " ").replace(",", "")
                # modified = re.search("\(.*short\)", new_cur)
                if new_cur == new_entry:
                  found = True
              # check = next((entry for entry in arr if entry["title"] == new_cur), None)
              if not found and "Looney Tunes" not in cur:
                print(cur)
            if possibility.text is not None and "Looney Tunes" not in cur:
              cur_title = possibility.text
              cur_wiki = possibility["href"]
              # info = re.search("\(.+\/.+\)", str(item))
              # cur_date = info.split("/")[1]
              # cur_producers = info.split("/")[0]

      if cur_title is not None and cur_title != "":
        # cur_episode = next((entry for entry in arr if entry["title"] == cur_title), None)
        wiki_episodes.append({
          # "date": cur_date,
          "title": cur_title,
          "wiki": base_wiki_url + cur_wiki,
          "series": cur_series,
          "public_domain": cur_pd,
          "blue_ribbon": cur_br,
          "censored_eleven": cur_c11
          # "producers": cur_producers
        })
        wiki_list_count += 1

      info = None
      cur_pd = False
      cur_br = False
      cur_c11 = False
      cur_date = None
      cur_title = None
      cur_series = None
      cur_episode = None
      cur_producers = None

  print("found %d wiki entries" % wiki_list_count)
  print("found %d public domain episodes" % pd_count)
  print("found %d blue ribbon episodes" % br_count)
  print("found %d censored eleven episodes" % c11_count)
  
  with open("wiki_episodes.json", "w") as write_file:
    json.dump(wiki_episodes, write_file, indent=4)
    # episode = {
    #   "summary": None,
    # }
  with open("episodes.json", "w") as write_file:
    json.dump(arr, write_file, indent=4)
# The Looney Tunes and Merrie Melodies Collection

This is a listing of the shorts, feature films, and television specials in Warner Bros.' Looney Tunes and Merrie Melodies series, extending from 1929 through the present. Over a thousand animated theatrical shorts alone were released under the Looney Tunes and Merrie Melodies banners from the 1930s through the 1960s.

## Looney Tunes

Looney Tunes is an American animated comedy short film series produced by Warner Bros. starting from 1930 to 1969, concurrently with its partner series Merrie Melodies, during the golden age of American animation.[2][3] The two series introduced a large cast of characters, including Bugs Bunny, Daffy Duck, and Porky Pig. The term Looney Tunes has since been expanded to also refer to the characters themselves.

## Merrie Melodies

Merrie Melodies is an American animated series of comedy short films produced by Warner Bros. starting in 1931, during the golden age of American animation, and ending in 1969. New Merrie Melodies cartoons were produced from the late 1970s to the late 1990s, as well as other made productions beginning in 1972. As with its sister series, Looney Tunes, it featured cartoon characters such as Bugs Bunny, Daffy Duck, Porky Pig, and Elmer Fudd.[1] Between 1934 and 1943, the Merrie Melodies series were distinguished from the black-and-white, Buddy or Porky Pig–starring Looney Tunes shorts by an emphasis on one-shot stories in color featuring Warner Bros.–owned musical selections. After Bugs Bunny became the breakout recurring star of Merrie Melodies, and Looney Tunes went to color in the early 1940s, the two series gradually lost their distinctions and shorts were assigned to each series more randomly.

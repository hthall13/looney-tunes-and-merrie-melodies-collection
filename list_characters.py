import requests
from bs4 import BeautifulSoup
from unidecode import unidecode

url = "https://looneytunes.fandom.com/wiki/Category:Looney_Tunes_Characters?from=Smokey+the+Genie"
html_text = requests.get(url).text
soup = BeautifulSoup(html_text, 'html.parser')
characters = []

print("parsing characters...")
for character in soup.find_all('a', class_="category-page__member-link"):
  if "Category:" not in character.text:
    char_dict = {
      "name": character.text,
      "episodes": []
    }
    characters.append(char_dict)
    # character_var = character.text.lower()
    # character_var = character_var.replace(" (character)", "")
    # character_var = character_var.replace(" (characters)", "")
    # character_var = character_var.replace(" (A Waggily Tale)", "")
    # character_var = character_var.replace(".","")
    # character_var = character_var.replace("'","")
    # character_var = character_var.replace(" ", "_")
    # character_var = character_var.replace("-","_")
    # print(character_var + "_episodes = []")
print(characters)
const streamlist = fuse.search(searchvar);

function create_video_info (episode) {
  let row = document.createElement("div");
  let container = document.createElement("div");
  let video_button_group = document.createElement("a");
  let number_button = document.createElement("button");
  let year_button = document.createElement("a");
  let video_title = document.createElement("h1");
  let video_date = document.createElement("h5");
  let video_summary = document.createElement("p");
  let video_characters = document.createElement("h6");
  
  // console.log("fetching video info for " + episode["title"] + "...");

  row.className = "row";
  container.className = "mx-auto";
  video_button_group.className = "btn-group";
  number_button.className = "btn btn-sm btn-outline-secondary disabled";
  year_button.className = "btn btn-sm btn-outline-secondary";
  video_title.className = "fw-light";
  video_date.className = "";
  video_summary.className = "lead text-muted";
  video_characters.className = "";

  video_button_group.setAttribute("id", "video-button-group");
  number_button.setAttribute("type", "button");
  year_button.setAttribute("href", "stream.html?y=" + episode["year"]);
  year_button.setAttribute("style", "width:100%");

  let video_date_object = new Date(episode["date"]);
  let video_date_string = months[video_date_object.getMonth()] + " " + (video_date_object.getDate() + 1) + ", " + video_date_object.getFullYear();
  video_date.appendChild(document.createTextNode(video_date_string));
  number_button.appendChild(document.createTextNode("#" + episode["number"]));
  year_button.appendChild(document.createTextNode(episode["year"]));
  video_summary.innerHTML = episode["summary"];

  video_button_group.appendChild(number_button);
  video_button_group.appendChild(year_button);

  container.appendChild(video_button_group);
  if (episode["wiki"] != "") {
    let wiki_link = document.createElement("a");
    wiki_link.setAttribute("href", episode["wiki"]);
    wiki_link.appendChild(document.createTextNode(episode["title"]));
    video_title.appendChild(wiki_link);
  } else {
    video_title.appendChild(document.createTextNode(episode["title"]));
  }
  container.appendChild(video_title);
  container.appendChild(video_date);
  container.appendChild(video_summary);
  if (episode["notes"] != "") {
    let video_notes = document.createElement("p");
    video_notes.className = "lead text-muted";
    video_notes.innerHTML = episode["notes"];
    container.appendChild(video_notes);
  }
  container.appendChild(video_characters);
  if (episode["production_number"] != "") {
    let production_number = document.createElement("small");
    production_number.className = "text-muted";
    production_number.appendChild(document.createTextNode("Production Number: " + episode["production_number"]));
    container.appendChild(production_number);
  }

  row.appendChild(container);

  // console.log("...done");
  
  return row;
};

function create_streamvid (video, first = false) {
  let source = document.createElement("source");
  if (first == true) {
    source.className = "active";
  }
  source.setAttribute("id", "source");
  source.setAttribute("src", video);
  source.setAttribute("type", "video/mp4");

  return source;
};

if (streamlist.length != 0) {
  streamlist.forEach(episode => {
    let episode_card = create_column(episode["item"]);
    sx_tbl.appendChild(episode_card);
  });
  streamlist.forEach((item, index) => {
    let vid;
    if (index == 0) {
      vid = create_streamvid(item.item.video, true);
      info = create_video_info(item.item);
    } else {
      vid = create_streamvid(item.item.video);
    }
    document.getElementById("video").append(vid);
    document.getElementById("video-info").append(info);
  });
}

var myvid = document.getElementById('video');
myvid.addEventListener('ended', function(e) {
  var activesource = document.querySelector("#video source.active");
  var nextsource = document.querySelector("#video source.active + source") || document.querySelector("#video source:first-child");

  // deactivate current source, and activate next one
  activesource.className = "";
  nextsource.className = "active";

  // update the video source and play
  myvid.src = nextsource.src;
  myvid.play();
});
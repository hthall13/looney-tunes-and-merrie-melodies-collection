const sx_tbl = document.getElementById("search-episode-table");
const searchbar = document.getElementById('searchbar');
const searchbutton = document.getElementById("searchbutton");
const streambutton = document.getElementById("streambutton");
const q = document.getElementById('query');
const year = findGetParameter("y");
const video = findGetParameter("v");
const producer = findGetParameter("p");
const character = findGetParameter("c");
const search = findGetParameter("s");
let fuse = new Fuse();
let searchvar = search;

// console.log("configuring search...");
function findGetParameter(parameterName) {
  var result = null,
      tmp = [];
  location.search
      .substring(1)
      .split("&")
      .forEach(function (item) {
        tmp = item.split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
      });
  return result;
}

if (year != null) {
  searchvar = year;
  fuse = new Fuse(data, {
    includeScore: true,
    keys: ['year'],
    threshold: 0
  });
} else if (video != null) {
  searchvar = video;
  fuse = new Fuse(data, {
    includeScore: true,
    keys: ['number'],
    threshold: 0
  });
} else if (producer != null) {
  searchvar = producer;
  fuse = new Fuse(data, {
    includeScore: true,
    keys: ['producer'],
    threshold: 0.5
  });
} else if (character != null) {
  searchvar = character;
  fuse = new Fuse(data, {
    includeScore: true,
    keys: ['characters'],
    threshold: 0.5
  });
} else if (search != null) {
  searchvar = search;
  fuse = new Fuse(data, {
    includeScore: true,
    keys: ["year", "number", "production_number", "title", "date", "summary", "notes", "producer"]
  });
} else {
  searchvar = q.value;
  fuse = new Fuse(data, {
    includeScore: true,
    keys: ["year", "number", "production_number", "title", "date", "summary", "notes", "producer"]
  });
}
// console.log("...done");

function submitted(event) {
  // console.log("search for: " + q.value + "...");
  event.preventDefault();
  sx_tbl.innerHTML = "";
  const result = fuse.search(q.value);
  // console.log("...found " + result.length + " episodes");
  result.forEach(episode => {
    let episode_card = create_column(episode["item"]);
    sx_tbl.appendChild(episode_card);
  });
  reveal('search')
}

function stream(event) {
  // console.log("rerouting to stream.html?s=" + q.value);
  event.preventDefault();
  if (q.value == "") {
    window.location.href = "stream.html?v=1";
  } else {
    window.location.href = "stream.html?s=" + q.value;
  }
  
}

searchbutton.addEventListener('click', submitted);
streambutton.addEventListener('click', stream);
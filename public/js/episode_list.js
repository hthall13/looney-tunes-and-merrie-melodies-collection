// console.log("loading episode list...");
let data = [
    {
        "year": "1940",
        "number": "1",
        "production_number": "42",
        "title": "Puss Gets the Boot",
        "date": "1940-02-10",
        "summary": "Tom and Jerry's first cartoon. Tom (here named Jasper) tries to stop the mouse Jerry (here named Jinx) from breaking plates and glasses before <a href=\"/wiki/Mammy_Two_Shoes\" title=\"Mammy Two Shoes\">Mammy Two Shoes</a> can kick Jasper out.",
        "notes": "First appearances of Tom (as Jasper), Jerry (as Jinx), and Mammy Two Shoes. First <i>Tom and Jerry</i> cartoon nominated for an <a href=\"/wiki/Academy_Awards\" title=\"Academy Awards\">Academy Award</a> for <a href=\"/wiki/Academy_Award_for_Best_Animated_Short_Film\" title=\"Academy Award for Best Animated Short Film\">Best Short Subject, Cartoon</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Puss_Gets_the_Boot",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E01%20-%20Puss%20Gets%20The%20Boot%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E01%20-%20Puss%20Gets%20The%20Boot%20%281080p%20BluRay%20x265%20Ghost%29_000537.jpg"
    },
    {
        "year": "1941",
        "number": "2",
        "production_number": "60",
        "title": "The Midnight Snack",
        "date": "1941-07-19",
        "summary": "Jerry attempts to outsmart Tom so he can get a snack from the refrigerator.",
        "notes": "First time Tom and Jerry are referred to by those names. Rereleased in Perspecta Stereo in 1958.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_Midnight_Snack",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E02%20-%20The%20Midnight%20Snack%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E02%20-%20The%20Midnight%20Snack%20%281080p%20BluRay%20x265%20Ghost%29_000528.jpg"
    },
    {
        "year": "1941",
        "number": "3",
        "production_number": "78",
        "title": "The Night Before Christmas (1941 film)",
        "date": "1941-12-06",
        "summary": "Tom gets to know the spirit of giving when he begins to feel guilty after blockading the front door, trapping Jerry outside in the cold on <a href=\"/wiki/Christmas_Eve\" title=\"Christmas Eve\">Christmas Eve</a>.",
        "notes": "Nominated for an <a href=\"/wiki/Academy_Awards\" title=\"Academy Awards\">Academy Award</a> for <a href=\"/wiki/Academy_Award_for_Best_Animated_Short_Film\" title=\"Academy Award for Best Animated Short Film\">Best Short Subjects, Cartoons</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_Night_Before_Christmas_(1941_film)",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E03%20-%20The%20Night%20Before%20Christmas%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E03%20-%20The%20Night%20Before%20Christmas%20%281080p%20BluRay%20x265%20Ghost%29_000498.jpg"
    },
    {
        "year": "1942",
        "number": "4",
        "production_number": "69",
        "title": "Fraidy Cat",
        "date": "1942-01-17",
        "summary": "Jerry plays tricks to scare the fur off of Tom.",
        "notes": "U.S. television print cuts out Mammy Two Shoes due to racially insensitive subject matter.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E04%20-%20Fraidy%20Cat%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E04%20-%20Fraidy%20Cat%20%281080p%20BluRay%20x265%20Ghost%29_000479.jpg"
    },
    {
        "year": "1942",
        "number": "5",
        "production_number": "64",
        "title": "Dog Trouble",
        "date": "1942-04-18",
        "summary": "Tom and Jerry team up to stop the Bulldog from mauling both of them.",
        "notes": "First appearance of <a href=\"/wiki/Spike_and_Tyke_(characters)\" title=\"Spike and Tyke (characters)\">Spike</a> as an Unnamed Bulldog.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Mammy Two Shoes"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E05%20-%20Dog%20Trouble%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E05%20-%20Dog%20Trouble%20%281080p%20BluRay%20x265%20Ghost%29_000457.jpg"
    },
    {
        "year": "1942",
        "number": "6",
        "production_number": "74",
        "title": "Puss n' Toots",
        "date": "1942-05-30",
        "summary": "Tom tries to woo a female cat.",
        "notes": "First appearance of <a href=\"/wiki/List_of_Tom_and_Jerry_characters#Toots\" title=\"List of Tom and Jerry characters\">Toots</a>. Rereleased in Perspecta Stereo in 1958.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E06%20-%20Puss%20N%27%20Toots%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E06%20-%20Puss%20N%27%20Toots%20%281080p%20BluRay%20x265%20Ghost%29_000429.jpg"
    },
    {
        "year": "1942",
        "number": "7",
        "production_number": "79",
        "title": "The Bowling Alley-Cat",
        "date": "1942-07-18",
        "summary": "Tom and Jerry chase each other around a bowling alley.",
        "notes": "First cartoon featuring a sport as its theme.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E07%20-%20The%20Bowling%20Alley-Cat%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E07%20-%20The%20Bowling%20Alley-Cat%20%281080p%20BluRay%20x265%20Ghost%29_000467.jpg"
    },
    {
        "year": "1942",
        "number": "8",
        "production_number": "81",
        "title": "Fine Feathered Friend",
        "date": "1942-10-10",
        "summary": "Jerry flees from Tom by hiding with a chicken family.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E08%20-%20Fine%20Feathered%20Friend%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E08%20-%20Fine%20Feathered%20Friend%20%281080p%20BluRay%20x265%20Ghost%29_000453.jpg"
    },
    {
        "year": "1943",
        "number": "9",
        "production_number": "85",
        "title": "Sufferin' Cats!",
        "date": "1943-01-16",
        "summary": "Tom competes with an alley cat (Meathead) to see who can catch Jerry\u00a0first.",
        "notes": "First appearance of <a href=\"/wiki/List_of_Tom_and_Jerry_characters#Meathead\" title=\"List of Tom and Jerry characters\">Meathead</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E09%20-%20Sufferin%27%20Cats%21%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E09%20-%20Sufferin%27%20Cats%21%20%281080p%20BluRay%20x265%20Ghost%29_000461.jpg"
    },
    {
        "year": "1943",
        "number": "10",
        "production_number": "89",
        "title": "The Lonesome Mouse",
        "date": "1943-05-22",
        "summary": "When Mammy Two Shoes kicks Tom out of the house after Jerry frames him, the mouse enjoys his freedom without Tom until he gets lonesome. They work together to prove Tom's worth as a mouse-catcher to Mammy.",
        "notes": "Rarely airs on <a href=\"/wiki/Cartoon_Network\" title=\"Cartoon Network\">Cartoon Network</a> and <a href=\"/wiki/Boomerang_(TV_network)\" title=\"Boomerang (TV network)\">Boomerang</a> due to a gag reference of <a href=\"/wiki/Adolf_Hitler\" title=\"Adolf Hitler\">Adolf Hitler</a>. An unusual short where Tom and Jerry speak.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E10%20-%20The%20Lonesome%20Mouse%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E10%20-%20The%20Lonesome%20Mouse%20%281080p%20BluRay%20x265%20Ghost%29_000468.jpg"
    },
    {
        "year": "1943",
        "number": "11",
        "production_number": "91",
        "title": "The Yankee Doodle Mouse",
        "date": "1943-06-26",
        "summary": "Jerry wages war with Tom from his \"cat raid shelter\" in the basement.",
        "notes": "First cartoon to win an <a href=\"/wiki/Academy_Awards\" title=\"Academy Awards\">Academy Award</a> for <a href=\"/wiki/Academy_Award_for_Best_Animated_Short_Film\" title=\"Academy Award for Best Animated Short Film\">Best Short Subject, Cartoon</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_Yankee_Doodle_Mouse",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E11%20-%20The%20Yankee%20Doodle%20Mouse%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E11%20-%20The%20Yankee%20Doodle%20Mouse%20%281080p%20BluRay%20x265%20Ghost%29_000438.jpg"
    },
    {
        "year": "1943",
        "number": "12",
        "production_number": "99",
        "title": "Baby Puss",
        "date": "1943-12-25",
        "summary": "Nancy dresses up Tom like a baby, prompting Jerry and Tom's feline friends to make fun of him.",
        "notes": "First appearance of <a href=\"/wiki/List_of_Tom_and_Jerry_characters#Butch\" title=\"List of Tom and Jerry characters\">Butch</a> and <a href=\"/wiki/List_of_Tom_and_Jerry_characters#Topsy\" title=\"List of Tom and Jerry characters\">Topsy</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Butch Isacc Cat"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E12%20-%20Baby%20Puss%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E12%20-%20Baby%20Puss%20%281080p%20BluRay%20x265%20Ghost%29_000460.jpg"
    },
    {
        "year": "1944",
        "number": "13",
        "production_number": "104",
        "title": "The Zoot Cat",
        "date": "1944-02-26",
        "summary": "Tom and Jerry try to impress Toots by wearing a <a href=\"/wiki/Zoot_suit\" title=\"Zoot suit\">zoot suit</a>.",
        "notes": "Unusual for a Tom and Jerry cartoon, characters speak lengthy lines.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_Zoot_Cat",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E13%20-%20The%20Zoot%20Cat%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E13%20-%20The%20Zoot%20Cat%20%281080p%20BluRay%20x265%20Ghost%29_000416.jpg"
    },
    {
        "year": "1944",
        "number": "14",
        "production_number": "109",
        "title": "The Million Dollar Cat",
        "date": "1944-05-06",
        "summary": "Tom inherits a million dollars on one condition: He must avoid causing harm to any animal, which Jerry uses to his advantage.",
        "notes": "<a href=\"/wiki/Scott_Bradley_(composer)\" title=\"Scott Bradley (composer)\">Scott Bradley</a> received the only music credit for this short, but examination of the archived orchestral score bears the inscription, \u2018Adapted by Ted Duncan\u2019. As Barrier has remarked in <i>Hollywood Cartoons</i>, this score is very unlike Bradley\u2019s other work of the period, since it \u2018sounds like ordinary dance-band music, related only tenuously to the cartoon action\u2019. It seems plausible that Duncan adapted the score from pre-existing songs because Bradley was unavailable, and the latter received credit for contractual reasons.<sup class=\"reference\" id=\"cite_ref-3\"><a href=\"#cite_note-3\">[3]</a></sup>",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E14%20-%20The%20Million%20Dollar%20Cat%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E14%20-%20The%20Million%20Dollar%20Cat%20%281080p%20BluRay%20x265%20Ghost%29_000415.jpg"
    },
    {
        "year": "1944",
        "number": "15",
        "production_number": "114",
        "title": "The Bodyguard",
        "date": "1944-07-22",
        "summary": "Jerry frees Spike the bulldog from the dog-catcher's truck. Spike promises to protect Jerry from Tom by responding to the sound of a whistle.",
        "notes": "First regular appearance of <a href=\"/wiki/Spike_and_Tyke_(characters)\" title=\"Spike and Tyke (characters)\">Spike</a>",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E15%20-%20The%20Bodyguard%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E15%20-%20The%20Bodyguard%20%281080p%20BluRay%20x265%20Ghost%29_000428.jpg"
    },
    {
        "year": "1944",
        "number": "16",
        "production_number": "117",
        "title": "Puttin' On the Dog",
        "date": "1944-10-28",
        "summary": "When Jerry hides in the dog pound, Tom disguises himself as a dog.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E16%20-%20Puttin%27%20On%20The%20Dog%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E16%20-%20Puttin%27%20On%20The%20Dog%20%281080p%20BluRay%20x265%20Ghost%29_000415.jpg"
    },
    {
        "year": "1944",
        "number": "17",
        "production_number": "118",
        "title": "Mouse Trouble",
        "date": "1944-11-23",
        "summary": "Tom reads a book consisting of tips for catching mice.",
        "notes": "Won an <a href=\"/wiki/Academy_Awards\" title=\"Academy Awards\">Academy Award</a> for <a href=\"/wiki/Academy_Award_for_Best_Animated_Short_Film\" title=\"Academy Award for Best Animated Short Film\">Best Short Subject, Cartoon</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Mouse_Trouble",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E17%20-%20Mouse%20Trouble%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E17%20-%20Mouse%20Trouble%20%281080p%20BluRay%20x265%20Ghost%29_000434.jpg"
    },
    {
        "year": "1945",
        "number": "18",
        "production_number": "123",
        "title": "The Mouse Comes to Dinner",
        "date": "1945-05-05",
        "summary": "Tom invites Toots to a dinner party.",
        "notes": "U.S. television print cuts out Mammy Two-Shoes due to additional racist stereotyping.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E18%20-%20The%20Mouse%20Comes%20To%20Dinner%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E18%20-%20The%20Mouse%20Comes%20To%20Dinner%20%281080p%20BluRay%20x265%20Ghost%29_000429.jpg"
    },
    {
        "year": "1945",
        "number": "19",
        "production_number": "132",
        "title": "Mouse in Manhattan",
        "date": "1945-07-07",
        "summary": "Jerry takes a trip to <a href=\"/wiki/Manhattan\" title=\"Manhattan\">Manhattan</a>.",
        "notes": "Tom has a cameo role in this cartoon.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E19%20-%20Mouse%20In%20Manhattan%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E19%20-%20Mouse%20In%20Manhattan%20%281080p%20BluRay%20x265%20Ghost%29_000474.jpg"
    },
    {
        "year": "1945",
        "number": "20",
        "production_number": "126",
        "title": "Tee for Two",
        "date": "1945-07-21",
        "summary": "Tom attempts to play golf, but Jerry ruins his fun.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Tee_for_Two",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E20%20-%20Tee%20For%20Two%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E20%20-%20Tee%20For%20Two%20%281080p%20BluRay%20x265%20Ghost%29_000410.jpg"
    },
    {
        "year": "1945",
        "number": "21",
        "production_number": "129",
        "title": "Flirty Birdy",
        "date": "1945-09-22",
        "summary": "Tom disguises himself as a female bird to trick an <a href=\"/wiki/Eagle\" title=\"Eagle\">eagle</a> who also wants to eat Jerry, which works <i>too</i> well.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E21%20-%20Flirty%20Birdy%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E21%20-%20Flirty%20Birdy%20%281080p%20BluRay%20x265%20Ghost%29_000423.jpg"
    },
    {
        "year": "1945",
        "number": "22",
        "production_number": "131",
        "title": "Quiet Please!",
        "date": "1945-12-22",
        "summary": "Spike threatens Tom to keep quiet during his nap, but Jerry is constantly making noise.",
        "notes": "Won an <a href=\"/wiki/Academy_Awards\" title=\"Academy Awards\">Academy Award</a> for <a href=\"/wiki/Academy_Award_for_Best_Animated_Short_Film\" title=\"Academy Award for Best Animated Short Film\">Best Short Subject, Cartoon</a>.<sup class=\"reference\" id=\"cite_ref-IndVallanceB_4-0\"><a href=\"#cite_note-IndVallanceB-4\">[4]</a></sup>",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E22%20-%20Quiet%20Please%21%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E22%20-%20Quiet%20Please%21%20%281080p%20BluRay%20x265%20Ghost%29_000443.jpg"
    },
    {
        "year": "1946",
        "number": "23",
        "production_number": "137",
        "title": "Springtime for Thomas",
        "date": "1946-03-30",
        "summary": "Tom falls in love with a new female cat, Toodles. Jerry tries to break them up by sending Tom's friend/enemy Butch to her.",
        "notes": "First appearance of <a href=\"/wiki/List_of_Tom_and_Jerry_characters#Toodles_Galore\" title=\"List of Tom and Jerry characters\">Toodles Galore</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Butch Isacc Cat",
            "Toodles Galore Lena Cat"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E23%20-%20Springtime%20For%20Thomas%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E23%20-%20Springtime%20For%20Thomas%20%281080p%20BluRay%20x265%20Ghost%29_000451.jpg"
    },
    {
        "year": "1946",
        "number": "24",
        "production_number": "142",
        "title": "The Milky Waif",
        "date": "1946-05-18",
        "summary": "Nibbles visits one night and wants some milk, so Jerry tries to steal some from Tom.",
        "notes": "First appearance of <a href=\"/wiki/Nibbles_(Tom_and_Jerry)\" title=\"Nibbles (Tom and Jerry)\">Nibbles</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Nibbles Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E24%20-%20The%20Milky%20Waif%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E24%20-%20The%20Milky%20Waif%20%281080p%20BluRay%20x265%20Ghost%29_000431.jpg"
    },
    {
        "year": "1946",
        "number": "25",
        "production_number": "145",
        "title": "Trap Happy",
        "date": "1946-06-29",
        "summary": "Tom calls a mouse exterminator (Butch) to get rid of Jerry.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Butch Isacc Cat"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S01E25%20-%20Trap%20Happy%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S01E25%20-%20Trap%20Happy%20%281080p%20BluRay%20x265%20Ghost%29_000416.jpg"
    },
    {
        "year": "1946",
        "number": "26",
        "production_number": "149",
        "title": "Solid Serenade",
        "date": "1946-08-31",
        "summary": "Tom sneaks up to Toodles' house to sing love songs to her at night.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Toodles Galore Lena Cat"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Solid_Serenade",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E01%20-%20Solid%20Serenade%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E01%20-%20Solid%20Serenade%20%281080p%20BluRay%20x265%20Ghost%29_000433.jpg"
    },
    {
        "year": "1947",
        "number": "27",
        "production_number": "155",
        "title": "Cat Fishin'",
        "date": "1947-02-22",
        "summary": "Tom goes fishing using Jerry as bait and deals with watchdog Spike.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E02%20-%20Cat%20Fishin%27%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E02%20-%20Cat%20Fishin%27%20%281080p%20BluRay%20x265%20Ghost%29_000451.jpg"
    },
    {
        "year": "1947",
        "number": "28",
        "production_number": "153",
        "title": "Part Time Pal",
        "date": "1947-03-15",
        "summary": "Mammy warns Tom to keep Jerry out of the refrigerator or she'll throw him out, but Tom accidentally becomes repeatedly drunk and befriends Jerry.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E03%20-%20Part%20Time%20Pal%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E03%20-%20Part%20Time%20Pal%20%281080p%20BluRay%20x265%20Ghost%29_000458.jpg"
    },
    {
        "year": "1947",
        "number": "29",
        "production_number": "165",
        "title": "The Cat Concerto",
        "date": "1947-04-26",
        "summary": "Pianist Tom performs <i><a href=\"/wiki/Hungarian_Rhapsody_No._2\" title=\"Hungarian Rhapsody No. 2\">Hungarian Rhapsody No. 2</a></i> by <a href=\"/wiki/Franz_Liszt\" title=\"Franz Liszt\">Franz Liszt</a> until Jerry breaks up his act.",
        "notes": "Won an <a href=\"/wiki/Academy_Awards\" title=\"Academy Awards\">Oscar</a> for <a href=\"/wiki/Academy_Award_for_Best_Animated_Short_Film\" title=\"Academy Award for Best Animated Short Film\">Best Short Subject, Cartoon</a>.<sup class=\"reference\" id=\"cite_ref-IndVallanceB_4-1\"><a href=\"#cite_note-IndVallanceB-4\">[4]</a></sup> In 1994, it was voted #42 of the 50 Greatest Cartoons of all time by members of the animation field, the only Tom & Jerry cartoon to make the list.<sup class=\"reference\" id=\"cite_ref-5\"><a href=\"#cite_note-5\">[5]</a></sup>",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_Cat_Concerto",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E04%20-%20The%20Cat%20Concerto%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E04%20-%20The%20Cat%20Concerto%20%281080p%20BluRay%20x265%20Ghost%29_000438.jpg"
    },
    {
        "year": "1947",
        "number": "30",
        "production_number": "157",
        "title": "Dr. Jekyll and Mr. Mouse",
        "date": "1947-06-14",
        "summary": "Tom tries to prevent Jerry from drinking his milk by poisoning it, but his plan completely backfires when the poison transforms Jerry into a monster.",
        "notes": "Nominated for an <a href=\"/wiki/Academy_Awards\" title=\"Academy Awards\">Oscar</a> for <a href=\"/wiki/Academy_Award_for_Best_Animated_Short_Film\" title=\"Academy Award for Best Animated Short Film\">Best Short Subject, Cartoon</a>. Original titles is rarely found on a 16mm Afga-Gevaert print with only one tiny splice at the Tom and Jerry card.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E05%20-%20Dr.%20Jekyll%20and%20Mr.%20Mouse%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E05%20-%20Dr.%20Jekyll%20and%20Mr.%20Mouse%20%281080p%20BluRay%20x265%20Ghost%29_000433.jpg"
    },
    {
        "year": "1947",
        "number": "31",
        "production_number": "158",
        "title": "Salt Water Tabby",
        "date": "1947-07-12",
        "summary": "Tom woos Toodles on the beach.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Toodles Galore Lena Cat"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E06%20-%20Salt%20Water%20Tabby%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E06%20-%20Salt%20Water%20Tabby%20%281080p%20BluRay%20x265%20Ghost%29_000426.jpg"
    },
    {
        "year": "1947",
        "number": "32",
        "production_number": "162",
        "title": "A Mouse in the House",
        "date": "1947-08-30",
        "summary": "Tom and Butch compete against each other to catch Jerry on Mammy Two Shoes' orders, but she ends up kicking out all three animals.",
        "notes": "Rarely seen on Cartoon Network and Boomerang due to perceived <a class=\"mw-redirect\" href=\"/wiki/Racial_abuse\" title=\"Racial abuse\">racial abuse</a> occurring in the end.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes",
            "Butch Isacc Cat"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E07%20-%20A%20Mouse%20In%20The%20House%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E07%20-%20A%20Mouse%20In%20The%20House%20%281080p%20BluRay%20x265%20Ghost%29_000462.jpg"
    },
    {
        "year": "1947",
        "number": "33",
        "production_number": "163",
        "title": "The Invisible Mouse",
        "date": "1947-09-27",
        "summary": "Jerry uses \"invisible ink\" to turn invisible and outsmart Tom.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E08%20-%20The%20Invisible%20Mouse%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E08%20-%20The%20Invisible%20Mouse%20%281080p%20BluRay%20x265%20Ghost%29_000428.jpg"
    },
    {
        "year": "1948",
        "number": "34",
        "production_number": "167",
        "title": "Kitty Foiled",
        "date": "1948-06-01",
        "summary": "<a href=\"/wiki/List_of_Tom_and_Jerry_characters#Cuckoo\" title=\"List of Tom and Jerry characters\">Cuckoo</a> saves Jerry from Tom.",
        "notes": "First appearance of <a href=\"/wiki/List_of_Tom_and_Jerry_characters#Cuckoo\" title=\"List of Tom and Jerry characters\">Cuckoo</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E09%20-%20Kitty%20Foiled%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E09%20-%20Kitty%20Foiled%20%281080p%20BluRay%20x265%20Ghost%29_000427.jpg"
    },
    {
        "year": "1948",
        "number": "35",
        "production_number": "173",
        "title": "The Truce Hurts",
        "date": "1948-07-17",
        "summary": "Tom, Jerry, and Spike (here called Butch) are fed up of fighting each other and call a truce, but the peace falls apart when they fight over a steak.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Butch Isacc Cat"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E10%20-%20The%20Truce%20Hurts%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E10%20-%20The%20Truce%20Hurts%20%281080p%20BluRay%20x265%20Ghost%29_000462.jpg"
    },
    {
        "year": "1948",
        "number": "36",
        "production_number": "172",
        "title": "Old Rockin' Chair Tom",
        "date": "1948-09-18",
        "summary": "Tom is briefly replaced by another cat, Lightning.",
        "notes": "First appearance of <a href=\"/wiki/List_of_Tom_and_Jerry_characters#Lightning\" title=\"List of Tom and Jerry characters\">Lightning</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E11%20-%20Old%20Rockin%27%20Chair%20Tom%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E11%20-%20Old%20Rockin%27%20Chair%20Tom%20%281080p%20BluRay%20x265%20Ghost%29_000436.jpg"
    },
    {
        "year": "1948",
        "number": "37",
        "production_number": "179",
        "title": "Professor Tom",
        "date": "1948-10-30",
        "summary": "Tom tries to teach his kitten student (Topsy) how to catch mice.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E12%20-%20Professor%20Tom%20%281080p%20BluRay%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E12%20-%20Professor%20Tom%20%281080p%20BluRay%20x265%20Ghost%29_000441.jpg"
    },
    {
        "year": "1948",
        "number": "38",
        "production_number": "182",
        "title": "Mouse Cleaning",
        "date": "1948-12-11",
        "summary": "After a muddy Tom chases Jerry through the house, Mammy Two Shoes forces the cat to clean the house. While she's gone, Jerry sabotages Tom's efforts.",
        "notes": "Blackface gag removed from television and omitted from DVD due to racial stereotyping.<sup class=\"reference\" id=\"cite_ref-tvshowsondvd.com_6-0\"><a href=\"#cite_note-tvshowsondvd.com-6\">[6]</a></sup>",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Mouse_Cleaning",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E13%20-%20Mouse%20Cleaning%20%28576p%20DVD%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E13%20-%20Mouse%20Cleaning%20%28576p%20DVD%20x265%20Ghost%29_000408.jpg"
    },
    {
        "year": "1949",
        "number": "39",
        "production_number": "184",
        "title": "Polka-Dot Puss",
        "date": "1949-02-26",
        "summary": "Tom convinces Mammy Two Shoes that he's too sick to go outside. He stays in the house until Jerry puts red dots all over Tom's face to trick him into thinking he's caught the <a href=\"/wiki/Measles\" title=\"Measles\">measles</a>.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E14%20-%20Polka-Dot%20Puss%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E14%20-%20Polka-Dot%20Puss%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000439.jpg"
    },
    {
        "year": "1949",
        "number": "40",
        "production_number": "191",
        "title": "The Little Orphan",
        "date": "1949-04-30",
        "summary": "In this <a href=\"/wiki/Thanksgiving\" title=\"Thanksgiving\">Thanksgiving</a> short, Jerry and Nibbles dine on Thanksgiving treats until Tom tries to stop them.",
        "notes": "Won an <a href=\"/wiki/Academy_Awards\" title=\"Academy Awards\">Oscar</a> for <a href=\"/wiki/Academy_Award_for_Best_Animated_Short_Film\" title=\"Academy Award for Best Animated Short Film\">Best Short Subject, Cartoon</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes",
            "Nibbles Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_Little_Orphan",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E15%20-%20The%20Little%20Orphan%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E15%20-%20The%20Little%20Orphan%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000435.jpg"
    },
    {
        "year": "1949",
        "number": "41",
        "production_number": "186",
        "title": "Hatch Up Your Troubles",
        "date": "1949-05-14",
        "summary": "Jerry protects a baby <a href=\"/wiki/Woodpecker\" title=\"Woodpecker\">woodpecker</a> from Tom until it finds its mother.",
        "notes": "First appearance of <a href=\"/wiki/List_of_Tom_and_Jerry_characters#Baby_Woodpecker\" title=\"List of Tom and Jerry characters\">the Baby Woodpecker</a>. Nominated for an <a href=\"/wiki/Academy_Awards\" title=\"Academy Awards\">Oscar</a> for <a href=\"/wiki/Academy_Award_for_Best_Animated_Short_Film\" title=\"Academy Award for Best Animated Short Film\">Best Short Subject, Cartoon</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E16%20-%20Hatch%20Up%20Your%20Troubles%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E16%20-%20Hatch%20Up%20Your%20Troubles%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000444.jpg"
    },
    {
        "year": "1949",
        "number": "42",
        "production_number": "189",
        "title": "Heavenly Puss",
        "date": "1949-07-09",
        "summary": "After a piano flattens Tom while he attempts to catch Jerry, Tom is refused entry to cat <a href=\"/wiki/Heaven\" title=\"Heaven\">heaven</a> due to his record of trying to harm Jerry. To save himself from <a href=\"/wiki/Hell\" title=\"Hell\">Hell</a>, Tom must have Jerry sign a certificate of forgiveness within one hour.",
        "notes": "Rarely airs in <a href=\"/wiki/Brazil\" title=\"Brazil\">Brazil</a><sup class=\"reference\" id=\"cite_ref-judao_7-0\"><a href=\"#cite_note-judao-7\">[7]</a></sup> and the <a href=\"/wiki/Middle_East\" title=\"Middle East\">Middle East</a> due to subplots involving <a href=\"/wiki/Damnation\" title=\"Damnation\">damnation</a> in <a href=\"/wiki/Hell\" title=\"Hell\">Hell</a>. Rereleased in Perspecta Stereo in 1956.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Butch Isacc Cat"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E17%20-%20Heavenly%20Puss%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E17%20-%20Heavenly%20Puss%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000446.jpg"
    },
    {
        "year": "1949",
        "number": "43",
        "production_number": "194",
        "title": "The Cat and the Mermouse",
        "date": "1949-09-03",
        "summary": "Tom chases a mermaid mouse who looks like Jerry.",
        "notes": "Rereleased in Perspecta Stereo in 1957.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E18%20-%20The%20Cat%20And%20The%20Mermouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E18%20-%20The%20Cat%20And%20The%20Mermouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000444.jpg"
    },
    {
        "year": "1949",
        "number": "44",
        "production_number": "197",
        "title": "Love That Pup",
        "date": "1949-10-01",
        "summary": "Jerry hides with <a href=\"/wiki/Spike_and_Tyke_(characters)\" title=\"Spike and Tyke (characters)\">Spike</a> and <a href=\"/wiki/Spike_and_Tyke_(characters)\" title=\"Spike and Tyke (characters)\">Tyke</a> so Tom will get in trouble if he tries to catch him.",
        "notes": "First appearance of <a href=\"/wiki/Spike_and_Tyke_(characters)\" title=\"Spike and Tyke (characters)\">Tyke</a> and <a href=\"/wiki/Daws_Butler\" title=\"Daws Butler\">Daws Butler</a>'s first time voicing Spike.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Tyke"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E19%20-%20Love%20That%20Pup%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E19%20-%20Love%20That%20Pup%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000442.jpg"
    },
    {
        "year": "1949",
        "number": "45",
        "production_number": "198",
        "title": "Jerry's Diary (1949 film)",
        "date": "1949-10-22",
        "summary": "Tom reads through Jerry's diary.",
        "notes": "Compilation short; contains footage from <i><a href=\"/wiki/Tee_for_Two\" title=\"Tee for Two\">Tee for Two</a></i>, <i><a href=\"/wiki/Mouse_Trouble\" title=\"Mouse Trouble\">Mouse Trouble</a></i>, <i><a href=\"/wiki/Solid_Serenade\" title=\"Solid Serenade\">Solid Serenade</a></i>, and <i><a href=\"/wiki/The_Yankee_Doodle_Mouse\" title=\"The Yankee Doodle Mouse\">The Yankee Doodle Mouse</a></i>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Jerry%27s_Diary_(1949_film)",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E20%20-%20Jerry%27s%20Diary%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E20%20-%20Jerry%27s%20Diary%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000381.jpg"
    },
    {
        "year": "1949",
        "number": "46",
        "production_number": "200",
        "title": "Tennis Chumps",
        "date": "1949-12-10",
        "summary": "Tom and Butch compete against each other in a game of tennis.",
        "notes": "Rereleased in Perspecta Stereo in 1957.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Butch Isacc Cat"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E21%20-%20Tennis%20Chumps%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E21%20-%20Tennis%20Chumps%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000379.jpg"
    },
    {
        "year": "1950",
        "number": "47",
        "production_number": "209",
        "title": "Little Quacker",
        "date": "1950-01-07",
        "summary": "Jerry protects a little duckling named Quacker from Tom.",
        "notes": "First appearances of <a href=\"/wiki/List_of_Tom_and_Jerry_characters#Quacker\" title=\"List of Tom and Jerry characters\">Quacker</a>, Henry, and Mama Duck. Rereleased in Perspecta Stereo in 1957.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Quacker"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E22%20-%20Little%20Quacker%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E22%20-%20Little%20Quacker%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000409.jpg"
    },
    {
        "year": "1950",
        "number": "48",
        "production_number": "206",
        "title": "Saturday Evening Puss",
        "date": "1950-01-14",
        "summary": "After Mammy Two Shoes goes out with her friends, Tom invites three of his feline friends: Butch, Lightning, and Topsy over for a party with loud music, which disturbs Jerry, who is trying to sleep.",
        "notes": "Only (albeit brief) time that the face of <a href=\"/wiki/Mammy_Two_Shoes\" title=\"Mammy Two Shoes\">Mammy Two Shoes</a> is shown. Rereleased in Perspecta Stereo in 1957. Rereleased to television in the mid-1960s with Mammy Two Shoes replaced by a new character (a slim white woman).",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes",
            "Butch Isacc Cat"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Saturday_Evening_Puss",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E23%20-%20Saturday%20Evening%20Puss%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E23%20-%20Saturday%20Evening%20Puss%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000356.jpg"
    },
    {
        "year": "1950",
        "number": "49",
        "production_number": "210",
        "title": "Texas Tom",
        "date": "1950-03-11",
        "summary": "Tom tries to woo a cowgirl cat.",
        "notes": "Rereleased in Perspecta Stereo in 1957.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E24%20-%20Texas%20Tom%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E24%20-%20Texas%20Tom%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000371.jpg"
    },
    {
        "year": "1950",
        "number": "50",
        "production_number": "201",
        "title": "Jerry and the Lion",
        "date": "1950-04-08",
        "summary": "Jerry promises to return an escaped circus lion to the African jungle.",
        "notes": "Only appearance of <a href=\"/wiki/List_of_Tom_and_Jerry_characters#Lion\" title=\"List of Tom and Jerry characters\">Lion</a>. Rereleased in Perspecta Stereo in 1957.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E25%20-%20Jerry%20And%20The%20Lion%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E25%20-%20Jerry%20And%20The%20Lion%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000403.jpg"
    },
    {
        "year": "1950",
        "number": "51",
        "production_number": "212",
        "title": "Safety Second",
        "date": "1950-07-01",
        "summary": "Jerry and Nibbles celebrate <a href=\"/wiki/Independence_Day_(United_States)\" title=\"Independence Day (United States)\">Independence Day</a>. Nibbles wants to set off firecrackers, but Jerry would rather play it safer.",
        "notes": "Rereleased in Perspecta Stereo in 1957.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Nibbles Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S02E26%20-%20Safety%20Second%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S02E26%20-%20Safety%20Second%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000379.jpg"
    },
    {
        "year": "1950",
        "number": "52",
        "production_number": "224",
        "title": "Tom and Jerry in the Hollywood Bowl",
        "date": "1950-09-16",
        "summary": "Tom conducts the overture of <a href=\"/wiki/Die_Fledermaus\" title=\"Die Fledermaus\">Die Fledermaus</a> by <a href=\"/wiki/Johann_Strauss_II\" title=\"Johann Strauss II\">Johann Strauss II</a> at the <a href=\"/wiki/Hollywood_Bowl\" title=\"Hollywood Bowl\">Hollywood Bowl</a>, but Jerry also wants to conduct.",
        "notes": "Rereleased in Perspecta Stereo in 1957.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E01%20-%20The%20Hollywood%20Bowl%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E01%20-%20The%20Hollywood%20Bowl%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000405.jpg"
    },
    {
        "year": "1950",
        "number": "53",
        "production_number": "214",
        "title": "The Framed Cat",
        "date": "1950-10-21",
        "summary": "When Tom steals a chicken drumstick and frames Jerry, Jerry gets even by stealing Spike's bone and framing Tom.",
        "notes": "Rereleased in Perspecta Stereo in 1956.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Mammy Two Shoes",
            "Tyke"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E02%20-%20The%20Framed%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E02%20-%20The%20Framed%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000410.jpg"
    },
    {
        "year": "1950",
        "number": "54",
        "production_number": "215",
        "title": "Cue Ball Cat",
        "date": "1950-11-25",
        "summary": "Tom and Jerry duel in a <a href=\"/wiki/Billiard_hall\" title=\"Billiard hall\">billiard hall</a>.",
        "notes": "Rereleased in Perspecta Stereo in 1956.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E03%20-%20Cue%20Ball%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E03%20-%20Cue%20Ball%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000407.jpg"
    },
    {
        "year": "1951",
        "number": "55",
        "production_number": "216",
        "title": "Casanova Cat",
        "date": "1951-01-06",
        "summary": "Tom offers Jerry as a gift to a wealthy and attractive female cat (Toodles). Jerry attracts the attention of another cat (Butch) who also becomes interested in her, resulting in a fight between Tom and the other cat for her affection.",
        "notes": "Blackface gag removed from television and omitted from DVD due to racial stereotyping.<sup class=\"reference\" id=\"cite_ref-tvshowsondvd.com_6-1\"><a href=\"#cite_note-tvshowsondvd.com-6\">[6]</a></sup> Rereleased in Perspecta Stereo in 1958.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Butch Isacc Cat",
            "Toodles Galore Lena Cat"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Casanova_Cat",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E04%20-%20Casanova%20Cat%20%28576p%20DVD%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E04%20-%20Casanova%20Cat%20%28576p%20DVD%20x265%20Ghost%29_000388.jpg"
    },
    {
        "year": "1951",
        "number": "56",
        "production_number": "219",
        "title": "Jerry and the Goldfish",
        "date": "1951-03-03",
        "summary": "Jerry must save a goldfish from Tom.",
        "notes": "Rereleased in Perspecta Stereo in 1958.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E05%20-%20Jerry%20And%20The%20Goldfish%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E05%20-%20Jerry%20And%20The%20Goldfish%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000422.jpg"
    },
    {
        "year": "1951",
        "number": "57",
        "production_number": "220",
        "title": "Jerry's Cousin",
        "date": "1951-04-07",
        "summary": "Jerry enlists help from his tough cousin Muscles to deal with Tom.",
        "notes": "Nominated for an <a href=\"/wiki/Academy_Awards\" title=\"Academy Awards\">Oscar</a> for <a href=\"/wiki/Academy_Award_for_Best_Animated_Short_Film\" title=\"Academy Award for Best Animated Short Film\">Academy Award for Short Subject, Cartoon</a>. First appearance of <a href=\"/wiki/List_of_Tom_and_Jerry_characters#Muscles\" title=\"List of Tom and Jerry characters\">Muscles Mouse</a>. Rereleased in Perspecta Stereo in 1958.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Butch Isacc Cat"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E06%20-%20Jerry%27s%20Cousin%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E06%20-%20Jerry%27s%20Cousin%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000391.jpg"
    },
    {
        "year": "1951",
        "number": "58",
        "production_number": "223",
        "title": "Sleepy-Time Tom",
        "date": "1951-05-26",
        "summary": "After staying out all night with his alley cat friends, Tom attempts to catch Jerry on Mammy Two Shoes' orders, but he gets sleepy in the process.",
        "notes": "Rereleased in Perspecta Stereo in 1958.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes",
            "Butch Isacc Cat"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E07%20-%20Sleepy-Time%20Tom%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E07%20-%20Sleepy-Time%20Tom%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000394.jpg"
    },
    {
        "year": "1951",
        "number": "59",
        "production_number": "227",
        "title": "His Mouse Friday",
        "date": "1951-07-07",
        "summary": "Tom becomes a castaway on an island and chases Jerry to a native village, but Jerry tricks the cat by disguising himself as a blackface native.",
        "notes": "Rereleased in Perspecta Stereo in 1958. This short is edited in two ways on <i>Festival of Fun</i> VHS and <i><a href=\"/wiki/Tom_and_Jerry_Spotlight_Collection\" title=\"Tom and Jerry Spotlight Collection\">Spotlight Collection</a></i> DVD.<sup class=\"reference\" id=\"cite_ref-8\"><a href=\"#cite_note-8\">[8]</a></sup>",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E08%20-%20His%20Mouse%20Friday%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E08%20-%20His%20Mouse%20Friday%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000372.jpg"
    },
    {
        "year": "1951",
        "number": "60",
        "production_number": "232",
        "title": "Slicked-up Pup",
        "date": "1951-09-08",
        "summary": "Spike threatens Tom to keep Tyke clean while he's gone. Jerry dirties Tyke to get Tom in trouble.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Tyke"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E09%20-%20Slicked-Up%20Pup%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E09%20-%20Slicked-Up%20Pup%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000364.jpg"
    },
    {
        "year": "1951",
        "number": "61",
        "production_number": "231",
        "title": "Nit-Witty Kitty",
        "date": "1951-10-06",
        "summary": "Mammy Two Shoes accidentally knocks Tom out with a blow to the head which causes him to forget who he is and think that he is a mouse, and Jerry finds Tom more obnoxious as a fellow rodent.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E10%20-%20Nit-Witty%20Kitty%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E10%20-%20Nit-Witty%20Kitty%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000371.jpg"
    },
    {
        "year": "1951",
        "number": "62",
        "production_number": "229",
        "title": "Cat Napping",
        "date": "1951-12-08",
        "summary": "Tom and Jerry fight over who's going to sleep in the hammock.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Tyke"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E11%20-%20Cat%20Napping%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E11%20-%20Cat%20Napping%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000404.jpg"
    },
    {
        "year": "1952",
        "number": "63",
        "production_number": "233",
        "title": "The Flying Cat",
        "date": "1952-01-12",
        "summary": "Tom chases Jerry and Cuckoo by devising an aerial plan of attack.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E12%20-%20The%20Flying%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E12%20-%20The%20Flying%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000393.jpg"
    },
    {
        "year": "1952",
        "number": "64",
        "production_number": "235",
        "title": "The Duck Doctor",
        "date": "1952-02-16",
        "summary": "Tom shoots down a wild duckling while hunting. Jerry helps him get airborne again.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E13%20-%20The%20Duck%20Doctor%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E13%20-%20The%20Duck%20Doctor%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000408.jpg"
    },
    {
        "year": "1952",
        "number": "65",
        "production_number": "247",
        "title": "The Two Mouseketeers",
        "date": "1952-03-15",
        "summary": "Jerry and Nibbles are hungry <a href=\"/wiki/The_Three_Musketeers\" title=\"The Three Musketeers\">Mouseketeers</a>, and Tom is a guard in charge of protecting the king's banquet.",
        "notes": "Rarely airs in Brazil due to the ending in which Tom gets <a href=\"/wiki/Guillotine\" title=\"Guillotine\">executed</a>.<sup class=\"reference\" id=\"cite_ref-judao_7-1\"><a href=\"#cite_note-judao-7\">[7]</a></sup> Won an <a href=\"/wiki/Academy_Awards\" title=\"Academy Awards\">Oscar</a> for <a href=\"/wiki/Academy_Award_for_Best_Animated_Short_Film\" title=\"Academy Award for Best Animated Short Film\">Best Short Subject, Cartoon</a>.<sup class=\"reference\" id=\"cite_ref-IndVallanceB_4-2\"><a href=\"#cite_note-IndVallanceB-4\">[4]</a></sup>",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Nibbles Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_Two_Mouseketeers",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E14%20-%20The%20Two%20Mouseketeers%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E14%20-%20The%20Two%20Mouseketeers%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000422.jpg"
    },
    {
        "year": "1952",
        "number": "66",
        "production_number": "240",
        "title": "Smitten Kitten",
        "date": "1952-04-12",
        "summary": "When Tom falls in love, Jerry's devil recalls the times when Tom fell in love and caused problems for Jerry.",
        "notes": "Compilation short; contains footage from <i>Salt Water Tabby</i>, <i>The Mouse Comes to Dinner</i>, <i>Texas Tom</i>, and <i><a href=\"/wiki/Solid_Serenade\" title=\"Solid Serenade\">Solid Serenade</a></i>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Toodles Galore Lena Cat"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E15%20-%20Smitten%20Kitten%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E15%20-%20Smitten%20Kitten%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000437.jpg"
    },
    {
        "year": "1952",
        "number": "67",
        "production_number": "238",
        "title": "Triplet Trouble",
        "date": "1952-04-19",
        "summary": "Mammy Two Shoes adopts three kittens who torment Tom and Jerry, so the two team up to have their revenge.",
        "notes": "First (official) appearance of kittens Fluff, Muff, and Puff.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E16%20-%20Triplet%20Trouble%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E16%20-%20Triplet%20Trouble%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000393.jpg"
    },
    {
        "year": "1952",
        "number": "68",
        "production_number": "242",
        "title": "Little Runaway",
        "date": "1952-06-14",
        "summary": "Tom intends to give an escaped seal pup back to the circus, but Jerry wants to help the seal pup escape.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E17%20-%20Little%20Runaway%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E17%20-%20Little%20Runaway%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000405.jpg"
    },
    {
        "year": "1952",
        "number": "69",
        "production_number": "243",
        "title": "Fit to Be Tied",
        "date": "1952-07-26",
        "summary": "After the passing of a new leash law, Tom torments Spike and uses the opportunity to chase Jerry, but Jerry has Spike protect him from Tom.",
        "notes": "Similar in story and spirit to <i><a class=\"mw-redirect\" href=\"/wiki/The_Bodyguard_(1944_film)\" title=\"The Bodyguard (1944 film)\">The Bodyguard</a></i>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Tyke"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E18%20-%20Fit%20To%20Be%20Tied%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E18%20-%20Fit%20To%20Be%20Tied%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000394.jpg"
    },
    {
        "year": "1952",
        "number": "70",
        "production_number": "244",
        "title": "Push-Button Kitty",
        "date": "1952-09-06",
        "summary": "Fed up with Tom's laziness, Mammy buys a new mouse-catching robot cat.",
        "notes": "Last appearance of Mammy Two Shoes, who was retired from the cartoons.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Mammy Two Shoes"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E19%20-%20Push-Button%20Kitty%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E19%20-%20Push-Button%20Kitty%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000366.jpg"
    },
    {
        "year": "1952",
        "number": "71",
        "production_number": "252",
        "title": "Cruise Cat",
        "date": "1952-10-18",
        "summary": "Tom is hired as a sailor tasked with keeping Jerry off a cruise ship.",
        "notes": "Contains footage from <i>Texas Tom</i>. Rereleased in Perspecta Stereo in 1958.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E20%20-%20Cruise%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E20%20-%20Cruise%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000407.jpg"
    },
    {
        "year": "1952",
        "number": "72",
        "production_number": "250",
        "title": "The Dog House",
        "date": "1952-11-29",
        "summary": "Spike decides to build his dream dog house, but Tom and Jerry's antics constantly destroy it.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Tyke"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E21%20-%20The%20Dog%20House%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E21%20-%20The%20Dog%20House%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000343.jpg"
    },
    {
        "year": "1953",
        "number": "73",
        "production_number": "254",
        "title": "The Missing Mouse",
        "date": "1953-01-10",
        "summary": "After Jerry is covered in white <a href=\"/wiki/Shoe_polish\" title=\"Shoe polish\">shoe polish</a>, he scares Tom into thinking that he is an explosive white mouse that escaped from a lab.",
        "notes": "Only <i>Tom and Jerry</i> cartoon scored by Edward Plumb because Scott Bradley was on vacation.<sup class=\"reference\" id=\"cite_ref-9\"><a href=\"#cite_note-9\">[9]</a></sup>",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E22%20-%20The%20Missing%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E22%20-%20The%20Missing%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000370.jpg"
    },
    {
        "year": "1953",
        "number": "74",
        "production_number": "256",
        "title": "Jerry and Jumbo",
        "date": "1953-02-21",
        "summary": "Jerry befriends a baby elephant named Jumbo and disguises him as a large mouse to mess with Tom.",
        "notes": "First appearance of Jumbo and his mother.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E23%20-%20Jerry%20And%20Jumbo%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E23%20-%20Jerry%20And%20Jumbo%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000408.jpg"
    },
    {
        "year": "1953",
        "number": "75",
        "production_number": "266",
        "title": "Johann Mouse",
        "date": "1953-03-21",
        "summary": "As the pet owned by <a class=\"mw-redirect\" href=\"/wiki/Johann_Strauss\" title=\"Johann Strauss\">Johann Strauss</a> in <a href=\"/wiki/Vienna\" title=\"Vienna\">Vienna</a>, Tom becomes an accomplished pianist himself after his master goes away in order to lure dancing Jerry out with piano music.",
        "notes": "Last cartoon in the series to win an <a href=\"/wiki/Academy_Awards\" title=\"Academy Awards\">Oscar</a> for <a href=\"/wiki/Academy_Award_for_Best_Animated_Short_Film\" title=\"Academy Award for Best Animated Short Film\">Best Short Subject, Cartoon</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Johann_Mouse",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E24%20-%20Johann%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E24%20-%20Johann%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000452.jpg"
    },
    {
        "year": "1953",
        "number": "76",
        "production_number": "260",
        "title": "That's My Pup!",
        "date": "1953-04-25",
        "summary": "Spike strikes an agreement with Tom for the feline to act scared whenever Tyke barks at him.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Tyke"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E25%20-%20That%27s%20My%20Pup%21%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E25%20-%20That%27s%20My%20Pup%21%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000426.jpg"
    },
    {
        "year": "1953",
        "number": "77",
        "production_number": "258",
        "title": "Just Ducky",
        "date": "1953-09-05",
        "summary": "After Quacker hatches, Jerry befriends him and teaches him how to swim so he can find his family, but Jerry must also protect him from Tom.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Quacker"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S03E26%20-%20Just%20Ducky%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S03E26%20-%20Just%20Ducky%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000390.jpg"
    },
    {
        "year": "1953",
        "number": "78",
        "production_number": "262",
        "title": "Two Little Indians",
        "date": "1953-10-17",
        "summary": "Jerry is a scoutmaster who is taking two young mice (both resembling Nibbles) on a hiking trip.",
        "notes": "Rarely airs on Cartoon Network and Boomerang because of <a class=\"mw-redirect\" href=\"/wiki/Native_American_stereotypes\" title=\"Native American stereotypes\">Native American stereotyping</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Tyke",
            "Nibbles Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E01%20-%20Two%20Little%20Indians%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E01%20-%20Two%20Little%20Indians%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000389.jpg"
    },
    {
        "year": "1953",
        "number": "79",
        "production_number": "264",
        "title": "Life with Tom",
        "date": "1953-11-21",
        "summary": "Jerry writes an autobiography titled <i>Life with Tom</i>, which Tom has mixed emotions reading.",
        "notes": "Compilation short; contains footage from <i>Cat Fishin'</i>, <i>The Little Orphan</i>, and <i>Kitty Foiled</i>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Butch Isacc Cat",
            "Tyke",
            "Nibbles Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E02%20-%20Life%20With%20Tom%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E02%20-%20Life%20With%20Tom%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000436.jpg"
    },
    {
        "year": "1954",
        "number": "80",
        "production_number": "275",
        "title": "Puppy Tale",
        "date": "1954-01-23",
        "summary": "A litter of puppies are thrown into a river, but Jerry saves them and has to deal with one that will not leave him and Tom alone.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E03%20-%20Puppy%20Tale%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E03%20-%20Puppy%20Tale%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000401.jpg"
    },
    {
        "year": "1954",
        "number": "81",
        "production_number": "268",
        "title": "Posse Cat",
        "date": "1954-01-30",
        "summary": "Tom is a cat owned by a western rancher living near the <a href=\"/wiki/La_Sal_Mountains\" title=\"La Sal Mountains\">La Sal Mountains</a>, who rules that, going forward, Tom's dinner will depend on him keeping Jerry out of the shack from stealing their food. Tom and Jerry eventually reach a truce that allows Tom to earn the meal.",
        "notes": "Similar in story and spirit to <i>Texas Tom</i>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E04%20-%20Posse%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E04%20-%20Posse%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000372.jpg"
    },
    {
        "year": "1954",
        "number": "82",
        "production_number": "270",
        "title": "Hic-cup Pup",
        "date": "1954-04-17",
        "summary": "Tom's usual antics of chasing Jerry wake Tyke up, and the puppy gets the hiccups. This annoys Spike, who threatens Tom to keep quiet, while Jerry tries to frame him.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Tyke"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E05%20-%20Hic-cup%20Pup%20%28480p%20DVD%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E05%20-%20Hic-cup%20Pup%20%28480p%20DVD%20x265%20Ghost%29_000362.jpg"
    },
    {
        "year": "1954",
        "number": "83",
        "production_number": "273",
        "title": "Little School Mouse",
        "date": "1954-05-29",
        "summary": "Jerry is a professor with a certified degree in outwitting cats, and tries to teach Nibbles how to do so, with very little success.",
        "notes": "Similar in story and spirit to <i>Professor Tom</i>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Nibbles Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E06%20-%20Little%20School%20Mouse%20%28480p%20DVD%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E06%20-%20Little%20School%20Mouse%20%28480p%20DVD%20x265%20Ghost%29_000410.jpg"
    },
    {
        "year": "1954",
        "number": "84",
        "production_number": "277",
        "title": "Baby Butch",
        "date": "1954-08-14",
        "summary": "Butch disguises himself as a <a class=\"mw-redirect\" href=\"/wiki/Baby\" title=\"Baby\">baby</a> to steal food from Tom and Jerry's household, aggravating both of them.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E07%20-%20Baby%20Butch%20%28480p%20DVD%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E07%20-%20Baby%20Butch%20%28480p%20DVD%20x265%20Ghost%29_000406.jpg"
    },
    {
        "year": "1954",
        "number": "85",
        "production_number": "279",
        "title": "Mice Follies",
        "date": "1954-09-04",
        "summary": "Jerry and Nibbles flood the kitchen and freeze it, turning it into a skating rink, causing Tom to use unusual tactics to catch them.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Nibbles Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E08%20-%20Mice%20Follies%20%28480p%20DVD%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E08%20-%20Mice%20Follies%20%28480p%20DVD%20x265%20Ghost%29_000406.jpg"
    },
    {
        "year": "1954",
        "number": "86",
        "production_number": "281",
        "title": "Neapolitan Mouse",
        "date": "1954-10-02",
        "summary": "Tom and Jerry vacation in <a href=\"/wiki/Naples\" title=\"Naples\">Naples</a> and encounter a local mouse named Topo.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E09%20-%20Neapolitan%20Mouse%20%28480p%20DVD%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E09%20-%20Neapolitan%20Mouse%20%28480p%20DVD%20x265%20Ghost%29_000421.jpg"
    },
    {
        "year": "1954",
        "number": "87",
        "production_number": "283",
        "title": "Downhearted Duckling",
        "date": "1954-11-13",
        "summary": "After reading the story of \"The Ugly Duckling\", Quacker is persistent with the idea of his being ugly, and even resorts to being eaten by Tom rather than to live with his \"ugliness\".",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Quacker"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E10%20-%20Downhearted%20Duckling%20%28480p%20DVD%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E10%20-%20Downhearted%20Duckling%20%28480p%20DVD%20x265%20Ghost%29_000372.jpg"
    },
    {
        "year": "1954",
        "number": "88",
        "production_number": "296",
        "title": "Pet Peeve",
        "date": "1954-11-20",
        "summary": "After the cost of dog and cat food increase, George and Joan (Tom and Spike's owners) decide they must get rid of one of them before they are eaten out of their home. Tom and Spike must compete to catch Jerry so they can stay, but both get kicked out in the end and Jerry stays.",
        "notes": "Produced simultaneously in both the standard Academy format and in widescreen CinemaScope. First appearances of <a href=\"/wiki/List_of_Tom_and_Jerry_characters#George_and_Joan\" title=\"List of Tom and Jerry characters\">George and Joan</a>, although their faces are not seen here.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Tyke"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E11%20-%20Pet%20Peeve%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E11%20-%20Pet%20Peeve%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000354.jpg"
    },
    {
        "year": "1954",
        "number": "89",
        "production_number": "294",
        "title": "Touch\u00e9, Pussy Cat!",
        "date": "1954-12-18",
        "summary": "Captain Jerry tries to teach eager Nibbles how to become a Mouseketeer.",
        "notes": "Produced simultaneously in both the standard Academy format and in widescreen CinemaScope. Last cartoon to get nominated for an <a href=\"/wiki/Academy_Awards\" title=\"Academy Awards\">Oscar</a> for <a href=\"/wiki/Academy_Award_for_Best_Animated_Short_Film\" title=\"Academy Award for Best Animated Short Film\">Best Short Subjects, Cartoons</a>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Nibbles Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E12%20-%20Touche%CC%81%2C%20Pussy%20Cat%21%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E12%20-%20Touche%CC%81%2C%20Pussy%20Cat%21%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000383.jpg"
    },
    {
        "year": "1955",
        "number": "90",
        "production_number": "298",
        "title": "Southbound Duckling",
        "date": "1955-03-12",
        "summary": "Quacker is determined to fly south for the winter, which Jerry objects since farm ducks do not fly south, while Tom tries to catch the duck.",
        "notes": "Produced simultaneously in both the standard Academy format and in CinemaScope.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Quacker"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E13%20-%20Southbound%20Duckling%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E13%20-%20Southbound%20Duckling%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000363.jpg"
    },
    {
        "year": "1955",
        "number": "91",
        "production_number": "285",
        "title": "Pup on a Picnic",
        "date": "1955-04-30",
        "summary": "Spike and Tyke are having a picnic, but several inconveniences occur.",
        "notes": "Produced simultaneously in both the standard Academy format and in CinemaScope.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Tyke"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E14%20-%20Pup%20On%20A%20Picnic%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E14%20-%20Pup%20On%20A%20Picnic%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000397.jpg"
    },
    {
        "year": "1955",
        "number": "92",
        "production_number": "287",
        "title": "Mouse for Sale",
        "date": "1955-05-21",
        "summary": "Tom sells Jerry disguising him as a white mouse after seeing an ad in the newspaper. But his plan to get rich backfires when the house owner finds the money and buys Jerry back.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E15%20-%20Mouse%20For%20Sale%20%28480p%20DVD%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E15%20-%20Mouse%20For%20Sale%20%28480p%20DVD%20x265%20Ghost%29_000401.jpg"
    },
    {
        "year": "1955",
        "number": "93",
        "production_number": "292",
        "title": "Designs on Jerry",
        "date": "1955-09-02",
        "summary": "<a href=\"/wiki/Stick_figure\" title=\"Stick figure\">Stick figure</a> versions of Tom and Jerry come to life when Tom creates a very detailed blueprint of a mousetrap.",
        "notes": "",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E16%20-%20Designs%20On%20Jerry%20%28480p%20DVD%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E16%20-%20Designs%20On%20Jerry%20%28480p%20DVD%20x265%20Ghost%29_000388.jpg"
    },
    {
        "year": "1955",
        "number": "94",
        "production_number": "299",
        "title": "Tom and Ch\u00e9rie",
        "date": "1955-09-09",
        "summary": "Mouseketeer Nibbles gets frustrated when Captain Mouseketeer Jerry repeatedly asks him to deliver his love letters despite Mouseketeer Nibbles's continually encountering troubles with Tom along the way.",
        "notes": "Produced in CinemaScope. This is the only Tom and Jerry episode during the Hanna-Barbera era where Tom and Jerry never come in contact with each other.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Nibbles Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E17%20-%20Tom%20And%20Che%CC%81rie%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E17%20-%20Tom%20And%20Che%CC%81rie%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000367.jpg"
    },
    {
        "year": "1955",
        "number": "95",
        "production_number": "297",
        "title": "Smarty Cat",
        "date": "1955-10-14",
        "summary": "Tom and his pals watch old footage of Spike's misery while the owners are not home.",
        "notes": "Compilation short; contains footage from <i><a href=\"/wiki/Solid_Serenade\" title=\"Solid Serenade\">Solid Serenade</a></i>, <i><a class=\"mw-redirect\" href=\"/wiki/Cat_Fishin%27\" title=\"Cat Fishin'\">Cat Fishin'</a></i>, and <i>Fit to Be Tied</i>.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Butch Isacc Cat",
            "Tyke",
            "Toodles Galore Lena Cat"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E18%20-%20Smarty%20Cat%20%28480p%20DVD%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E18%20-%20Smarty%20Cat%20%28480p%20DVD%20x265%20Ghost%29_000398.jpg"
    },
    {
        "year": "1955",
        "number": "96",
        "production_number": "289",
        "title": "Pecos Pest",
        "date": "1955-11-11",
        "summary": "Jerry's uncle Pecos comes to the city with his guitar for his television singing debut. Tom is terrified of Pecos because he keeps using Tom's whiskers as replacement guitar strings.",
        "notes": "Only appearance of <a href=\"/wiki/List_of_Tom_and_Jerry_characters#Uncle_Pecos\" title=\"List of Tom and Jerry characters\">Uncle Pecos</a>. Last <i>Tom and Jerry</i> cartoon released in the standard <a class=\"mw-redirect\" href=\"/wiki/Academy_format\" title=\"Academy format\">Academy format</a>. All subsequent Hanna-Barbera cartoons were released in CinemaScope. Last <i>Tom and Jerry</i> cartoon released with <a href=\"/wiki/Fred_Quimby\" title=\"Fred Quimby\">Fred Quimby</a> as producer.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E19%20-%20Pecos%20Pest%20%28480p%20DVD%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E19%20-%20Pecos%20Pest%20%28480p%20DVD%20x265%20Ghost%29_000384.jpg"
    },
    {
        "year": "1955",
        "number": "97",
        "production_number": "300",
        "title": "That's My Mommy",
        "date": "1955-11-19",
        "summary": "Quacker hatches near Tom and <a href=\"/wiki/Imprinting_(psychology)\" title=\"Imprinting (psychology)\">imprints</a> on him, thinking Tom is his mother, despite Jerry's multiple pleas to show him otherwise.",
        "notes": "Produced in CinemaScope. First <i>Tom and Jerry</i> cartoon with <a href=\"/wiki/William_Hanna\" title=\"William Hanna\">William Hanna</a> and <a href=\"/wiki/Joseph_Barbera\" title=\"Joseph Barbera\">Joseph Barbera</a> as both producers and directors.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Quacker"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E20%20-%20That%27s%20My%20Mommy%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E20%20-%20That%27s%20My%20Mommy%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000334.jpg"
    },
    {
        "year": "1956",
        "number": "98",
        "production_number": "301",
        "title": "The Flying Sorceress",
        "date": "1956-01-27",
        "summary": "Tom sees an advert wanting an intelligent cat as a travel companion. He leaves his home for the new job, only to find a creepy house occupied by a witch, who wants a cat to take on broomstick rides.",
        "notes": "Produced in CinemaScope.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E21%20-%20The%20Flying%20Sorceress%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E21%20-%20The%20Flying%20Sorceress%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000377.jpg"
    },
    {
        "year": "1956",
        "number": "99",
        "production_number": "314",
        "title": "The Egg and Jerry",
        "date": "1956-03-23",
        "summary": "A mother woodpecker leaves for lunch leaving her egg behind, but the egg ends up in Jerry's home and hatches. The baby woodpecker <a href=\"/wiki/Imprinting_(psychology)\" title=\"Imprinting (psychology)\">thinks Jerry is his mother</a> and saves him from Tom.",
        "notes": "Produced in CinemaScope. CinemaScope remake of <i><a class=\"mw-redirect\" href=\"/wiki/Hatch_Up_Your_Troubles\" title=\"Hatch Up Your Troubles\">Hatch Up Your Troubles</a></i> and first of the three CinemaScope remakes.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E22%20-%20The%20Egg%20And%20Jerry%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E22%20-%20The%20Egg%20And%20Jerry%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000425.jpg"
    },
    {
        "year": "1956",
        "number": "100",
        "production_number": "303",
        "title": "Busy Buddies",
        "date": "1956-05-04",
        "summary": "When Jeannie the babysitter is too busy on the phone to look after the baby who is constantly crawling away, Tom and Jerry collaborate to make sure the baby does not get hurt.",
        "notes": "First appearance of <a href=\"/wiki/List_of_Tom_and_Jerry_characters#Jeannie_and_the_Baby\" title=\"List of Tom and Jerry characters\">Jeannie and the Baby</a>. Produced in CinemaScope.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E23%20-%20Busy%20Buddies%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E23%20-%20Busy%20Buddies%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000360.jpg"
    },
    {
        "year": "1956",
        "number": "101",
        "production_number": "304",
        "title": "Muscle Beach Tom",
        "date": "1956-09-07",
        "summary": "Tom arrives at the beach with a female cat to spend some quality time. But instead, he is competing with Butch by lifting weights to impress her.",
        "notes": "Produced in CinemaScope.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Butch Isacc Cat"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E24%20-%20Muscle%20Beach%20Tom%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E24%20-%20Muscle%20Beach%20Tom%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000381.jpg"
    },
    {
        "year": "1956",
        "number": "102",
        "production_number": "305",
        "title": "Down Beat Bear",
        "date": "1956-10-21",
        "summary": "A dancing bear escapes from the zoo and arrives at Tom and Jerry's house, so Jerry keeps playing music to make him dance with Tom and prevent Tom from calling to collect the reward.",
        "notes": "Produced in CinemaScope.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E25%20-%20Down%20Beat%20Bear%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E25%20-%20Down%20Beat%20Bear%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000375.jpg"
    },
    {
        "year": "1956",
        "number": "103",
        "production_number": "306",
        "title": "Blue Cat Blues",
        "date": "1956-11-16",
        "summary": "Jerry, narrating, recounts the tragic love story that led to Tom's depression.",
        "notes": "Rarely airs on Cartoon Network and Boomerang due to references of <a href=\"/wiki/Alcoholism\" title=\"Alcoholism\">alcoholism</a> and <a href=\"/wiki/Suicide\" title=\"Suicide\">suicide</a>. Produced in CinemaScope.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Butch Isacc Cat"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Blue_Cat_Blues",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E26%20-%20Blue%20Cat%20Blues%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E26%20-%20Blue%20Cat%20Blues%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000393.jpg"
    },
    {
        "year": "1956",
        "number": "104",
        "production_number": "307",
        "title": "Barbecue Brawl",
        "date": "1956-12-14",
        "summary": "Spike shows his son Tyke how to barbecue, but they have to deal with constant interruptions.",
        "notes": "Produced in CinemaScope and Perspecta Stereo.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike",
            "Tyke"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S04E27%20-%20Barbecue%20Brawl%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S04E27%20-%20Barbecue%20Brawl%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000377.jpg"
    },
    {
        "year": "1957",
        "number": "105",
        "production_number": "318",
        "title": "Tops with Pops",
        "date": "1957-02-22",
        "summary": "Jerry hides with Spike and Tyke so Tom will get in trouble if he tries to catch him.",
        "notes": "Produced in CinemaScope and Perspecta Stereo. CinemaScope remake version of <i><a class=\"mw-redirect\" href=\"/wiki/Love_That_Pup\" title=\"Love That Pup\">Love That Pup</a></i> and second of the three Cinemascope remakes.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E01%20-%20Tops%20With%20Pops%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E01%20-%20Tops%20With%20Pops%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000423.jpg"
    },
    {
        "year": "1957",
        "number": "106",
        "production_number": "308",
        "title": "Timid Tabby",
        "date": "1957-04-19",
        "summary": "Tom's cousin George comes to visit, and he's afraid of mice.",
        "notes": "Produced in CinemaScope and Perspecta Stereo.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E02%20-%20Timid%20Tabby%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E02%20-%20Timid%20Tabby%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000381.jpg"
    },
    {
        "year": "1957",
        "number": "107",
        "production_number": "321",
        "title": "Feedin' the Kiddie",
        "date": "1957-06-07",
        "summary": "Jerry and Tuffy dine on Thanksgiving treats until Tom tries to stop them.",
        "notes": "Produced in CinemaScope and Perspecta Stereo. Remake of <i><a href=\"/wiki/The_Little_Orphan\" title=\"The Little Orphan\">The Little Orphan</a></i> with Nibbles  named as Tuffy and is Jerry's nephew.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Nibbles Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E03%20-%20Feedin%27%20The%20Kiddie%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E03%20-%20Feedin%27%20The%20Kiddie%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000404.jpg"
    },
    {
        "year": "1957",
        "number": "108",
        "production_number": "310",
        "title": "Mucho Mouse",
        "date": "1957-09-06",
        "summary": "Tom is a mouse-catching world champion and arrives in <a href=\"/wiki/Spain\" title=\"Spain\">Spain</a> to catch Jerry, known as El Magnifico, but he miserably fails to catch him.",
        "notes": "Produced in CinemaScope and Perspecta Stereo.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Butch Isacc Cat"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E04%20-%20Mucho%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E04%20-%20Mucho%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000408.jpg"
    },
    {
        "year": "1957",
        "number": "109",
        "production_number": "311",
        "title": "Tom's Photo Finish",
        "date": "1957-11-01",
        "summary": "When Tom eats his owner's chicken and frames Spike, Jerry takes a picture to expose him, spreading copies around the house for his owners to see them. Tom goes to extreme measures to destroy or otherwise hide the photos from his owners, but ultimately fails.",
        "notes": "Produced in CinemaScope and Perspecta Stereo.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E05%20-%20Tom%27s%20Photo%20Finish%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E05%20-%20Tom%27s%20Photo%20Finish%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000355.jpg"
    },
    {
        "year": "1958",
        "number": "110",
        "production_number": "309",
        "title": "Happy Go Ducky",
        "date": "1958-01-03",
        "summary": "The <a href=\"/wiki/Easter_Bunny\" title=\"Easter Bunny\">Easter Bunny</a> leaves an <a href=\"/wiki/Easter_egg\" title=\"Easter egg\">Easter egg</a> for Tom and Jerry, which hatches into Quacker who thoroughly annoys them.",
        "notes": "Produced in CinemaScope and Perspecta Stereo.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Quacker"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E06%20-%20Happy%20Go%20Ducky%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E06%20-%20Happy%20Go%20Ducky%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000357.jpg"
    },
    {
        "year": "1958",
        "number": "111",
        "production_number": "317",
        "title": "Royal Cat Nap",
        "date": "1958-03-07",
        "summary": "Royal guard Tom must get rid of Mouseketeers Jerry and Tuffy without waking up the king from his nap.",
        "notes": "Produced in CinemaScope and Perspecta Stereo.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Nibbles Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E07%20-%20Royal%20Cat%20Nap%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E07%20-%20Royal%20Cat%20Nap%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000386.jpg"
    },
    {
        "year": "1958",
        "number": "112",
        "production_number": "325",
        "title": "The Vanishing Duck",
        "date": "1958-05-02",
        "summary": "In a plot reminiscent of 1947's <i>The Invisible Mouse</i>, Jerry and Quacker become invisible using vanishing cream and play pranks on Tom.",
        "notes": "Produced in CinemaScope and Perspecta Stereo.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Quacker"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E08%20-%20The%20Vanishing%20Duck%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E08%20-%20The%20Vanishing%20Duck%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000391.jpg"
    },
    {
        "year": "1958",
        "number": "113",
        "production_number": "329",
        "title": "Robin Hoodwinked",
        "date": "1958-06-06",
        "summary": "After <a href=\"/wiki/Robin_Hood\" title=\"Robin Hood\">Robin Hood</a> gets locked up, Jerry and Tuffy attempt to save him, but first they must get past Tom.",
        "notes": "Produced in CinemaScope and Perspecta Stereo.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Nibbles Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E09%20-%20Robin%20Hoodwinked%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E09%20-%20Robin%20Hoodwinked%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000366.jpg"
    },
    {
        "year": "1958",
        "number": "114",
        "production_number": "330",
        "title": "Tot Watchers",
        "date": "1958-08-01",
        "summary": "Due to Jeanine the babysitter's carelessness, Tom and Jerry must once again keep the baby from harm every time it gets loose.",
        "notes": "Rarely airs in the Middle East for its humor based on <a href=\"/wiki/Child_neglect\" title=\"Child neglect\">child neglect</a>. Produced in CinemaScope and Perspecta Stereo.",
        "producer": "Hanna-Barbera/MGM",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Tot_Watchers",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E10%20-%20Tot%20Watchers%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E10%20-%20Tot%20Watchers%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000380.jpg"
    },
    {
        "year": "1961",
        "number": "115",
        "production_number": "",
        "title": "Switchin' Kitten",
        "date": "1961-09-07",
        "summary": "",
        "notes": "",
        "producer": "Gene Deitch/Rembrandt Films",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Switchin%27_Kitten",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E11%20-%20Switchin%27%20Kitten%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E11%20-%20Switchin%27%20Kitten%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000502.jpg"
    },
    {
        "year": "1961",
        "number": "116",
        "production_number": "",
        "title": "Down and Outing",
        "date": "1961-10-26",
        "summary": "",
        "notes": "",
        "producer": "Gene Deitch/Rembrandt Films",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Down_and_Outing",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E12%20-%20Down%20and%20Outing%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E12%20-%20Down%20and%20Outing%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000397.jpg"
    },
    {
        "year": "1961",
        "number": "117",
        "production_number": "",
        "title": "It's Greek to Me-ow!",
        "date": "1961-12-07",
        "summary": "",
        "notes": "",
        "producer": "Gene Deitch/Rembrandt Films",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/It%27s_Greek_to_Me-ow!",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E13%20-%20It%27s%20Greek%20to%20Me-Ow%21%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E13%20-%20It%27s%20Greek%20to%20Me-Ow%21%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000397.jpg"
    },
    {
        "year": "1962",
        "number": "118",
        "production_number": "",
        "title": "High Steaks",
        "date": "1962-03-23",
        "summary": "",
        "notes": "",
        "producer": "Gene Deitch/Rembrandt Films",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/High_Steaks",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E14%20-%20High%20Steaks%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E14%20-%20High%20Steaks%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000373.jpg"
    },
    {
        "year": "1962",
        "number": "119",
        "production_number": "",
        "title": "Mouse into Space",
        "date": "1962-04-13",
        "summary": "",
        "notes": "",
        "producer": "Gene Deitch/Rembrandt Films",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Mouse_into_Space",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E15%20-%20Mouse%20into%20Space%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E15%20-%20Mouse%20into%20Space%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000387.jpg"
    },
    {
        "year": "1962",
        "number": "120",
        "production_number": "",
        "title": "Landing Stripling",
        "date": "1962-05-18",
        "summary": "",
        "notes": "",
        "producer": "Gene Deitch/Rembrandt Films",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Landing_Stripling",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E16%20-%20Landing%20Stripling%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E16%20-%20Landing%20Stripling%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000379.jpg"
    },
    {
        "year": "1962",
        "number": "121",
        "production_number": "",
        "title": "Calypso Cat",
        "date": "1962-06-21",
        "summary": "",
        "notes": "Rarely seen in the Middle East for mild suggestive humor as <a href=\"/wiki/Censorship_in_Saudi_Arabia#Film_and_television\" title=\"Censorship in Saudi Arabia\">outlawed in MENA regions</a>.",
        "producer": "Gene Deitch/Rembrandt Films",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Calypso_Cat",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E17%20-%20Calypso%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E17%20-%20Calypso%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000441.jpg"
    },
    {
        "year": "1962",
        "number": "122",
        "production_number": "",
        "title": "Dicky Moe",
        "date": "1962-07-20",
        "summary": "",
        "notes": "",
        "producer": "Gene Deitch/Rembrandt Films",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E18%20-%20Dicky%20Moe%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E18%20-%20Dicky%20Moe%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000406.jpg"
    },
    {
        "year": "1962",
        "number": "123",
        "production_number": "",
        "title": "The Tom and Jerry Cartoon Kit",
        "date": "1962-08-10",
        "summary": "",
        "notes": "",
        "producer": "Gene Deitch/Rembrandt Films",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_Tom_and_Jerry_Cartoon_Kit",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E19%20-%20The%20Tom%20and%20Jerry%20Cartoon%20Kit%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E19%20-%20The%20Tom%20and%20Jerry%20Cartoon%20Kit%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000377.jpg"
    },
    {
        "year": "1962",
        "number": "124",
        "production_number": "",
        "title": "Tall in the Trap",
        "date": "1962-09-14",
        "summary": "",
        "notes": "",
        "producer": "Gene Deitch/Rembrandt Films",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Tall_in_the_Trap",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E20%20-%20Tall%20in%20the%20Trap%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E20%20-%20Tall%20in%20the%20Trap%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000439.jpg"
    },
    {
        "year": "1962",
        "number": "125",
        "production_number": "",
        "title": "Sorry Safari",
        "date": "1962-10-12",
        "summary": "",
        "notes": "",
        "producer": "Gene Deitch/Rembrandt Films",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E21%20-%20Sorry%20Safari%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E21%20-%20Sorry%20Safari%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000416.jpg"
    },
    {
        "year": "1962",
        "number": "126",
        "production_number": "",
        "title": "Buddies Thicker Than Water",
        "date": "1962-11-01",
        "summary": "",
        "notes": "Shortened in <a href=\"/wiki/United_Kingdom\" title=\"United Kingdom\">United Kingdom</a> due to Tom and Jerry getting <a class=\"mw-redirect\" href=\"/wiki/Drunkeness\" title=\"Drunkeness\">drunk</a> on <a href=\"/wiki/Champagne\" title=\"Champagne\">champagne</a> in one scene.",
        "producer": "Gene Deitch/Rembrandt Films",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Buddies_Thicker_Than_Water",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E22%20-%20Buddies%20Thicker%20Than%20Water%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E22%20-%20Buddies%20Thicker%20Than%20Water%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000518.jpg"
    },
    {
        "year": "1962",
        "number": "127",
        "production_number": "",
        "title": "Carmen Get It!",
        "date": "1962-12-21",
        "summary": "",
        "notes": "",
        "producer": "Gene Deitch/Rembrandt Films",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Carmen_Get_It!",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S05E23%20-%20Carmen%20Get%20It%21%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S05E23%20-%20Carmen%20Get%20It%21%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000440.jpg"
    },
    {
        "year": "1963",
        "number": "128",
        "production_number": "",
        "title": "Pent-House Mouse",
        "date": "1963-07-27",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Pent-House_Mouse",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E01%20-%20Pent-House%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E01%20-%20Pent-House%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000411.jpg"
    },
    {
        "year": "1964",
        "number": "129",
        "production_number": "",
        "title": "The Cat Above and the Mouse Below",
        "date": "1964-02-25",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_Cat_Above_and_the_Mouse_Below",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E02%20-%20The%20Cat%20Above%20and%20the%20Mouse%20Below%20%28480p%20DVD%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E02%20-%20The%20Cat%20Above%20and%20the%20Mouse%20Below%20%28480p%20DVD%20x265%20Ghost%29_000379.jpg"
    },
    {
        "year": "1964",
        "number": "130",
        "production_number": "",
        "title": "Is There a Doctor in the Mouse?",
        "date": "1964-03-24",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Is_There_a_Doctor_in_the_Mouse%3F",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E03%20-%20Is%20There%20a%20Doctor%20in%20the%20Mouse%EF%BC%9F%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E03%20-%20Is%20There%20a%20Doctor%20in%20the%20Mouse%EF%BC%9F%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000412.jpg"
    },
    {
        "year": "1964",
        "number": "131",
        "production_number": "",
        "title": "Much Ado About Mousing",
        "date": "1964-04-14",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Much_Ado_About_Mousing",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E04%20-%20Much%20Ado%20About%20Mousing%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E04%20-%20Much%20Ado%20About%20Mousing%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000401.jpg"
    },
    {
        "year": "1964",
        "number": "132",
        "production_number": "",
        "title": "Snowbody Loves Me",
        "date": "1964-05-12",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Snowbody_Loves_Me",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E05%20-%20Snowbody%20Loves%20Me%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E05%20-%20Snowbody%20Loves%20Me%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000453.jpg"
    },
    {
        "year": "1964",
        "number": "133",
        "production_number": "",
        "title": "The Unshrinkable Jerry Mouse",
        "date": "1964-12-08",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_Unshrinkable_Jerry_Mouse",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E06%20-%20The%20Unshrinkable%20Jerry%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E06%20-%20The%20Unshrinkable%20Jerry%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000388.jpg"
    },
    {
        "year": "1965",
        "number": "134",
        "production_number": "",
        "title": "Ah, Sweet Mouse-Story of Life",
        "date": "1965-01-20",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Ah,_Sweet_Mouse-Story_of_Life",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E07%20-%20Ah%2C%20Sweet%20Mouse-Story%20of%20Life%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E07%20-%20Ah%2C%20Sweet%20Mouse-Story%20of%20Life%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000330.jpg"
    },
    {
        "year": "1965",
        "number": "135",
        "production_number": "",
        "title": "Tom-ic Energy",
        "date": "1965-01-27",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Tom-ic_Energy",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E08%20-%20Tom-ic%20Energy%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E08%20-%20Tom-ic%20Energy%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000383.jpg"
    },
    {
        "year": "1965",
        "number": "136",
        "production_number": "",
        "title": "Bad Day at Cat Rock",
        "date": "1965-02-10",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Bad_Day_at_Cat_Rock",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E09%20-%20Bad%20Day%20at%20Cat%20Rock%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E09%20-%20Bad%20Day%20at%20Cat%20Rock%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000346.jpg"
    },
    {
        "year": "1965",
        "number": "137",
        "production_number": "",
        "title": "The Brothers Carry-Mouse-Off",
        "date": "1965-03-03",
        "summary": "",
        "notes": "Directed by <a href=\"/wiki/Jim_Pabian\" title=\"Jim Pabian\">Jim Pabian</a>.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_Brothers_Carry-Mouse-Off",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E10%20-%20The%20Brothers%20Carry-Mouse-Off%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E10%20-%20The%20Brothers%20Carry-Mouse-Off%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000388.jpg"
    },
    {
        "year": "1965",
        "number": "138",
        "production_number": "",
        "title": "Haunted Mouse",
        "date": "1965-03-24",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Haunted_Mouse",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E11%20-%20Haunted%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E11%20-%20Haunted%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000399.jpg"
    },
    {
        "year": "1965",
        "number": "139",
        "production_number": "",
        "title": "I'm Just Wild About Jerry",
        "date": "1965-04-07",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/I%27m_Just_Wild_About_Jerry",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E12%20-%20I%27m%20Just%20Wild%20About%20Jerry%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E12%20-%20I%27m%20Just%20Wild%20About%20Jerry%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000372.jpg"
    },
    {
        "year": "1965",
        "number": "140",
        "production_number": "",
        "title": "Of Feline Bondage",
        "date": "1965-05-19",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Of_Feline_Bondage",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E13%20-%20Of%20Feline%20Bondage%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E13%20-%20Of%20Feline%20Bondage%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000362.jpg"
    },
    {
        "year": "1965",
        "number": "141",
        "production_number": "",
        "title": "The Year of the Mouse",
        "date": "1965-06-09",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_Year_of_the_Mouse",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E14%20-%20The%20Year%20of%20the%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E14%20-%20The%20Year%20of%20the%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000355.jpg"
    },
    {
        "year": "1965",
        "number": "142",
        "production_number": "",
        "title": "The Cat's Me-Ouch!",
        "date": "1965-12-22",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_Cat%27s_Me-Ouch!",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E15%20-%20The%20Cat%27s%20Me-Ouch%20%28480p%20DVD%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E15%20-%20The%20Cat%27s%20Me-Ouch%20%28480p%20DVD%20x265%20Ghost%29_000347.jpg"
    },
    {
        "year": "1966",
        "number": "143",
        "production_number": "",
        "title": "Duel Personality",
        "date": "1966-01-20",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Duel_Personality",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E16%20-%20Duel%20Personality%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E16%20-%20Duel%20Personality%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000356.jpg"
    },
    {
        "year": "1966",
        "number": "144",
        "production_number": "",
        "title": "Jerry, Jerry, Quite Contrary",
        "date": "1966-02-17",
        "summary": "",
        "notes": "Rarely airs on <a class=\"mw-redirect\" href=\"/wiki/Spacetoon_TV\" title=\"Spacetoon TV\">Spacetoon</a> and <a class=\"mw-redirect\" href=\"/wiki/MBC3\" title=\"MBC3\">MBC3</a> for the humor's dark undertone.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Jerry,_Jerry,_Quite_Contrary",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E17%20-%20Jerry%2C%20Jerry%2C%20Quite%20Contrary%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E17%20-%20Jerry%2C%20Jerry%2C%20Quite%20Contrary%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000383.jpg"
    },
    {
        "year": "1966",
        "number": "145",
        "production_number": "",
        "title": "Jerry-Go-Round",
        "date": "1966-03-03",
        "summary": "",
        "notes": "Directed by Abe Levitow.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Jerry-Go-Round",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E18%20-%20Jerry-Go-Round%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E18%20-%20Jerry-Go-Round%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000361.jpg"
    },
    {
        "year": "1966",
        "number": "146",
        "production_number": "",
        "title": "Love Me, Love My Mouse",
        "date": "1966-04-28",
        "summary": "",
        "notes": "Directed by <a href=\"/wiki/Chuck_Jones\" title=\"Chuck Jones\">Chuck Jones</a> and <a href=\"/wiki/Ben_Washam\" title=\"Ben Washam\">Ben Washam</a>. Last appearance of Toodles.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Toodles Galore Lena Cat"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Love_Me,_Love_My_Mouse",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E19%20-%20Love%20Me%2C%20Love%20My%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E19%20-%20Love%20Me%2C%20Love%20My%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000358.jpg"
    },
    {
        "year": "1966",
        "number": "147",
        "production_number": "",
        "title": "Puss 'n' Boats",
        "date": "1966-05-05",
        "summary": "",
        "notes": "Directed by <a href=\"/wiki/Abe_Levitow\" title=\"Abe Levitow\">Abe Levitow</a>.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Puss_%27n%27_Boats",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E20%20-%20Puss%20%27N%27%20Boats%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E20%20-%20Puss%20%27N%27%20Boats%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000369.jpg"
    },
    {
        "year": "1966",
        "number": "148",
        "production_number": "",
        "title": "Filet Meow",
        "date": "1966-06-30",
        "summary": "",
        "notes": "Directed by Abe Levitow.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Filet_Meow",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E21%20-%20Filet%20Meow%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E21%20-%20Filet%20Meow%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000367.jpg"
    },
    {
        "year": "1966",
        "number": "149",
        "production_number": "",
        "title": "Matinee Mouse",
        "date": "1966-07-14",
        "summary": "",
        "notes": "Direction credited to William Hanna and Joseph Barbera, with story and supervision by <a href=\"/wiki/Tom_Ray\" title=\"Tom Ray\">Tom Ray</a>. Compilation short; contains footage from <i>The Flying Cat</i>, <i>Professor Tom</i>, <i>The Missing Mouse</i>, <i>Jerry and the Lion</i>, <i>Love That Pup</i>, <i>The Flying Sorceress</i>, <i><a class=\"mw-redirect\" href=\"/wiki/Jerry%27s_Diary_(1949_film)\" title=\"Jerry's Diary (1949 film)\">Jerry's Diary</a></i>, and <i>The Truce Hurts</i>. Tom and Jerry watch themselves in a theater.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse",
            "Spike"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Matinee_Mouse",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E22%20-%20Matinee%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E22%20-%20Matinee%20Mouse%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000339.jpg"
    },
    {
        "year": "1966",
        "number": "150",
        "production_number": "",
        "title": "The A-Tom-inable Snowman",
        "date": "1966-08-04",
        "summary": "",
        "notes": "Directed by Abe Levitow.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_A-Tom-inable_Snowman",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E23%20-%20The%20A-TOM-iNABLE%20Snowman%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E23%20-%20The%20A-TOM-iNABLE%20Snowman%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000372.jpg"
    },
    {
        "year": "1966",
        "number": "151",
        "production_number": "",
        "title": "Catty-Cornered",
        "date": "1966-09-08",
        "summary": "",
        "notes": "Directed by Abe Levitow.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Catty-Cornered",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E24%20-%20Catty-Cornered%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E24%20-%20Catty-Cornered%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000369.jpg"
    },
    {
        "year": "1967",
        "number": "152",
        "production_number": "",
        "title": "Cat and Dupli-cat",
        "date": "1967-01-20",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Cat_and_Dupli-cat",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E25%20-%20Cat%20and%20Dupli-Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E25%20-%20Cat%20and%20Dupli-Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000384.jpg"
    },
    {
        "year": "1967",
        "number": "153",
        "production_number": "",
        "title": "O-Solar Meow",
        "date": "1967-02-24",
        "summary": "",
        "notes": "Directed by Abe Levitow.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/O-Solar_Meow",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E26%20-%20O-Solar-Meow%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E26%20-%20O-Solar-Meow%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000384.jpg"
    },
    {
        "year": "1967",
        "number": "154",
        "production_number": "",
        "title": "Guided Mouse-ille",
        "date": "1967-03-10",
        "summary": "",
        "notes": "Directed by Abe Levitow. Followup to <i>O-Solar-Meow</i>.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Guided_Mouse-ille",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E27%20-%20Guided%20Mouse-ille%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E27%20-%20Guided%20Mouse-ille%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000388.jpg"
    },
    {
        "year": "1967",
        "number": "155",
        "production_number": "",
        "title": "Rock 'n' Rodent",
        "date": "1967-04-07",
        "summary": "",
        "notes": "Directed by Abe Levitow.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Rock_%27n%27_Rodent",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E28%20-%20Rock%20%27N%27%20Rodent%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E28%20-%20Rock%20%27N%27%20Rodent%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000375.jpg"
    },
    {
        "year": "1967",
        "number": "156",
        "production_number": "",
        "title": "Cannery Rodent",
        "date": "1967-04-14",
        "summary": "",
        "notes": "",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Cannery_Rodent",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E29%20-%20Cannery%20Rodent%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E29%20-%20Cannery%20Rodent%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000382.jpg"
    },
    {
        "year": "1967",
        "number": "157",
        "production_number": "",
        "title": "The Mouse from H.U.N.G.E.R.",
        "date": "1967-04-21",
        "summary": "",
        "notes": "Directed by Abe Levitow.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/The_Mouse_from_H.U.N.G.E.R.",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E30%20-%20The%20Mouse%20from%20H.U.N.G.E.R.%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E30%20-%20The%20Mouse%20from%20H.U.N.G.E.R.%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000390.jpg"
    },
    {
        "year": "1967",
        "number": "158",
        "production_number": "",
        "title": "Surf-Bored Cat",
        "date": "1967-05-05",
        "summary": "",
        "notes": "Directed by Abe Levitow.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Surf-Bored_Cat",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E31%20-%20Surf-Bored%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E31%20-%20Surf-Bored%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000373.jpg"
    },
    {
        "year": "1967",
        "number": "159",
        "production_number": "",
        "title": "Shutter Bugged Cat",
        "date": "1967-06-23",
        "summary": "",
        "notes": "Direction credited to William Hanna and Joseph Barbera, with story and supervision by Tom Ray. Compilation short; Contains footage from <i><a class=\"mw-redirect\" href=\"/wiki/Part_Time_Pal\" title=\"Part Time Pal\">Part Time Pal</a></i>, <i><a href=\"/wiki/The_Yankee_Doodle_Mouse\" title=\"The Yankee Doodle Mouse\">The Yankee Doodle Mouse</a></i>, <i><a class=\"mw-redirect\" href=\"/wiki/Nit-Witty_Kitty\" title=\"Nit-Witty Kitty\">Nit-Witty Kitty</a></i>, <i><a href=\"/wiki/Johann_Mouse\" title=\"Johann Mouse\">Johann Mouse</a></i>, <i><a class=\"mw-redirect\" href=\"/wiki/Heavenly_Puss\" title=\"Heavenly Puss\">Heavenly Puss</a></i>, and <i><a class=\"mw-redirect\" href=\"/wiki/Designs_on_Jerry\" title=\"Designs on Jerry\">Designs on Jerry</a></i>.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Shutter_Bugged_Cat",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E32%20-%20Shutter-Bugged%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E32%20-%20Shutter-Bugged%20Cat%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000398.jpg"
    },
    {
        "year": "1967",
        "number": "160",
        "production_number": "",
        "title": "Advance and Be Mechanized",
        "date": "1967-08-25",
        "summary": "",
        "notes": "Directed by Ben Washam. Followup to <i>O-Solar-Meow</i>.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Advance_and_Be_Mechanized",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E33%20-%20Advance%20and%20Be%20Mechanized%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E33%20-%20Advance%20and%20Be%20Mechanized%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000340.jpg"
    },
    {
        "year": "1967",
        "number": "161",
        "production_number": "",
        "title": "Purr-Chance to Dream",
        "date": "1967-09-08",
        "summary": "",
        "notes": "Directed by Ben Washam. Followup to <i>The Cat's Me-Ouch</i>.",
        "producer": "Chuck Jones/Sib Tower 12",
        "characters": [
            "Tom Cat",
            "Jerry Mouse"
        ],
        "wiki": "https://en.wikipedia.org/wiki/Purr-Chance_to_Dream",
        "video": "https://archive.org/download/tom_and_jerry_full_1080p/Tom%20%26%20Jerry%20%281940%29%20-%20S06E34%20-%20Purr-Chance%20to%20Dream%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29.mp4",
        "thumbnail": "https://archive.org/download/tom_and_jerry_full_1080p/tom_and_jerry_full_1080p.thumbs/Tom%20%26%20Jerry%20%281940%29%20-%20S06E34%20-%20Purr-Chance%20to%20Dream%20%281080p%20AMZN%20WEB-DL%20x265%20Ghost%29_000344.jpg"
    }
];
// console.log("...done");